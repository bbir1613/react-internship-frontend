import HomeComponent from '../../components/HomeComponent';
import LoginComponent from '../../components/login';
import NotFoundComponent from '../../components/NotFoundComponent';
import RegisterPage from '../../pages/user/register';
import UserPage from '../../pages/user';
import UserJobDetailsPage from '../../pages/user/job';
import UserProfilePage from '../../pages/user/profile';
import AdminPage from '../../pages/admin';
import SkillPage from '../../pages/admin/skill';
import CompanyPage from '../../pages/company';
import CompanyUserAppliedForJobDetailsPage from '../../pages/company/jobs/userapplied';
import CompanyJobsPage from '../../pages/company/jobs';
import UserJobsAppliedPage from '../../pages/user/job/applied';
import * as UsersRole from '../roles';
import * as RoutesName from '../routes-name';
import LoginFormComponent from "../../components/login/redux-form";
import ReduxListFormTest from "../../components/ReduxListFormTest";

export const isLoggedIn = (user) => user.isLoggedIn;
export const isSysAdmin = (user) => user.userRoleId === UsersRole.SYS_ADMIN;
export const isUser = (user) => user.userRoleId === UsersRole.USER;
export const isCompany = (user) => user.userRoleId === UsersRole.COMPANY_USER;
export const defaultPage = (user) => {
    switch (user.userRoleId) {
        case UsersRole.SYS_ADMIN:
            return RoutesName.ADMIN_PAGE;
        case UsersRole.COMPANY_USER:
            return RoutesName.COMPANY_PAGE;
        case UsersRole.USER:
            return RoutesName.USER_PAGE;
        default:
            console.error(" INVALID PARAMETER ", user);
            return RoutesName.ANY;
    }

};
const routes = [
    {
        path: RoutesName.HOME,
        show: (user) => true,
        name: undefined,
        component: HomeComponent,
    },
    {
        path: "/loginform",
        show: (user) => !isLoggedIn(user),
        name: undefined,
        component: LoginFormComponent,
    },
    {
        path: "/redux",
        show: (user) => !isLoggedIn(user),
        name: undefined,
        component: ReduxListFormTest,
    },
    {
        path: RoutesName.LOGIN,
        show: (user) => !isLoggedIn(user),
        name: undefined,
        component: LoginComponent,
    },
    {
        path: RoutesName.REGISTER_PAGE,
        show: (user) => !isLoggedIn(user),
        name: undefined,
        component: RegisterPage,
    },
    {
        path: RoutesName.USER_PAGE,
        show: (user) => isLoggedIn(user) && isUser(user),
        name: 'Jobs',
        component: UserPage,
    },
    {
        path: RoutesName.USER_JOB,
        show: (user) => isLoggedIn(user) && isUser(user),
        name: undefined, // use this for a job details later
        component: UserJobDetailsPage,// on table click this page is shown, presents job details
    },
    {
        path: RoutesName.USER_JOB_APPLIED,
        show: (user) => isLoggedIn(user) && isUser(user),
        name: 'Applied Jobs', // use this for a job details later
        component: UserJobsAppliedPage,// on table click this page is shown, presents job details
    },
    {
        path: RoutesName.USER_PROFILE,
        show: (user) => isLoggedIn(user) && (isUser(user) || isCompany(user)),
        name: undefined,
        component: UserProfilePage,
    },
    {
        path: RoutesName.ADMIN_PAGE,
        show: (user) => isLoggedIn(user) && isSysAdmin(user),
        name: "Users",//CRUD USERS
        component: AdminPage,
    },
    {
        path: RoutesName.ADMIN_SKILL_PAGE,
        show: (user) => isLoggedIn(user) && isSysAdmin(user),
        name: "Skills",//CRUD USERS
        component: SkillPage,
    },
    {
        path: RoutesName.COMPANY_PAGE,
        show: (user) => isLoggedIn(user) && isCompany(user),
        name: 'Companies',
        component: CompanyPage,
    },
    {
        path: RoutesName.COMPANY_JOBS_PAGE,
        show: (user) => isLoggedIn(user) && isCompany(user),
        name: 'Jobs',
        component: CompanyJobsPage,
    },
    {
        path: RoutesName.COMPANY_JOB_PAGE,
        show: (user) => isLoggedIn(user) && isCompany(user),
        name: undefined,
        component: CompanyJobsPage,
    },
    {
        path: RoutesName.COMPANY_JOB,
        show: (user) => isLoggedIn(user) && isCompany(user),
        name: undefined,// on table click this page is shown, presents users that apply for current job
        component: CompanyUserAppliedForJobDetailsPage,
    },
    {
        path: RoutesName.ANY,
        show: () => true,
        name: undefined,
        component: NotFoundComponent,
    }
];

export default routes;