export const USERS = '/users';
export const COMPANIES = '/companies';
export const JOBS = '/jobs';
export const USERJOBAPPLICATIONS = '/userjobapplications';
export const JOBSKILLS = '/jobskills';
export const JOBBENEFITS = '/jobbenefits';
export const JOBREQUIREMENT = '/jobrequirements';
