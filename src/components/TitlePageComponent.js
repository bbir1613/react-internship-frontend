import React, {Fragment} from 'react';
import classNames from 'classnames';
import Title from './TitleComponent';
import AddButtonComponent from './AddButtonComponent';
import Typography from 'material-ui/Typography';
import Divider from 'material-ui/Divider';
import {withStyles} from 'material-ui/styles';

const styles = {
    flex: {
        display: 'flex',

    },
    marginTop: {
        marginTop: 10,
    },
    margin: {
        margin: 10,
    },
    grow: {
        flex: '1'
    },
};

const TitlePageComponent = (props) => {
    const {classes, title, subtitle, open, onClick} = props;

    return (
        <Fragment>
            <Title title={title}/>
            <Divider/>
            <div className={classNames(classes.flex, classes.margin)}>
                <div className={classes.grow}>
                    <Typography variant="display1"> {subtitle} </Typography>
                </div>
                <div>
                    <AddButtonComponent onClick={onClick}
                                        open={open}
                    />
                </div>
            </div>
            <Divider/>
        </Fragment>
    )
};

export default withStyles(styles)(TitlePageComponent);