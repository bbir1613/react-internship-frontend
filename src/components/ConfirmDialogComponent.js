import React from 'react';
import Button from 'material-ui/Button';
import Dialog, {
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from 'material-ui/Dialog';
import {withStyles} from 'material-ui/styles';

const styles = {
    root: {},
    table: {}
};

const ConfirmDialogComponent = (props) => {
    const {classes, open, info, title = "Are you sure ?", onAgree, onDisagree} = props;

    return (
        <div className={classes.root}>
            <Dialog
                open={open}
                onClose={onDisagree}
            >
                <DialogTitle>
                    {title}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        {info}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={onDisagree}>
                        Disagree
                    </Button>
                    <Button onClick={onAgree}>
                        Agree
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )

};

export default withStyles(styles)(ConfirmDialogComponent);