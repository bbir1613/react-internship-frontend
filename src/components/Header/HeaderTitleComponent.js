import React, {Fragment} from 'react';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button'
import {withStyles} from 'material-ui/styles';
import {Link} from 'react-router-dom';
import routes, {isLoggedIn} from '../../constants/routes';
import {withRouter} from 'react-router-dom';

const styles = {};

const HeaderTitleComponent = (props) => {
    const {user, logout, history} = props;
    return (
        <div>
            <Typography align='center' variant='title'>
                react internship
            </Typography>
            {isLoggedIn(user) ?
                <Typography variant="subheading" gutterBottom>
                    <Fragment>
                        {routes.map((route, index) => {
                            if (route.show(user) && route.name)
                                return <Button key={index} size='small' component={Link}
                                               to={route.path}> {route.name}</Button>;
                            return null;
                        })}
                        <Button size='small' onClick={() => logout(history)}> Logout </Button>
                    </Fragment>
                </Typography>
                :
                null
            }
        </div>
    )
};

export default withStyles(styles)(withRouter(HeaderTitleComponent));