import React from 'react';
import IconButton from 'material-ui/IconButton';
import Button from 'material-ui/Button'
import AccountCircle from 'material-ui-icons/AccountCircle';
import {withStyles} from 'material-ui/styles';
import {Link} from 'react-router-dom';
import {LOGIN} from '../../constants/routes-name';
import {isLoggedIn, isUser} from '../../constants/routes';
import {translate} from 'react-i18next';
import Flag from 'react-world-flags'
import {saveLanguage, getLanguage} from '../../util/storage'

const styles = {};

const HeaderRightMenu = (props) => {
    const {user, i18n} = props;
    const currentLanguage = getLanguage();
    
    const changeLanguage =()=>{
        if(currentLanguage === 'GBR'){
            i18n.changeLanguage('ro');
            saveLanguage('ro') 
        }else{
            i18n.changeLanguage('en'); 
            saveLanguage('GBR')
        }
    }

    if (!isLoggedIn(user)) {
        return (
            <div>
                <Button size='small' onClick={changeLanguage}>
                <Flag code={currentLanguage === 'ro'? 'GBR': 'ro'} height="16" />
                </Button>
                <Button size='small' component={Link} to={LOGIN}>Login</Button>
            </div>
        )
    }
    if (isUser(user)) {
        return (
            <IconButton component={Link} to={`/user/profile/${user.id}`} color="inherit">
                <AccountCircle/>
            </IconButton>
        )
    }
    return <div/>
};

export default translate('translations')(withStyles(styles)(HeaderRightMenu));