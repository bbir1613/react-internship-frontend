import React from 'react';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import IconButton from 'material-ui/IconButton';
import Home from 'material-ui-icons/Home';
import HeaderTitleComponent from './HeaderTitleComponent';
import HeaderRightMenu from './HeaderRightMenu';
import {withStyles} from 'material-ui/styles';
import {Link} from 'react-router-dom';
import {HOME} from '../../constants/routes-name';
// import logo from '../../resources/logo.jpg';

const styles = {
    root: {},
    toolbar: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    bigAvatar: {
        width: 60,
        height: 60,
    },
    logo: {
        borderRadius: '50%',
        width: 60,
        height: 60,
    }
};

const HeaderComponent = (props) => {
    const {classes, user, logout} = props;
    return (
        <div className={classes.root}>
            <AppBar position="static" color="default">
                <Toolbar className={classes.toolbar}>
                    <IconButton component={Link} to={HOME} color="inherit">
                        <Home/>
                    </IconButton>
                    <div>
                        <HeaderTitleComponent user={user} logout={logout}/>
                    </div>
                    <HeaderRightMenu user={user}/>
                </Toolbar>
            </AppBar>
        </div>
    )
};

export default withStyles(styles)(HeaderComponent);