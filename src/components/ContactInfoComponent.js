import React, {Fragment} from 'react';
import TextField from 'material-ui/TextField';
import {withStyles} from 'material-ui/styles';
import _ from 'lodash';

const styles = {
    input: {
        margin: 10,
    }
};

const ContactInfoComponent = (props) => {
    const {classes, countries, contactInfo, useValue = false} = props;

    const fieldProps = (fieldProps) => {
        const {label, className = classes.input} = fieldProps;
        const name = _.get(fieldProps, "name", label);
        const path = _.get(fieldProps, "path", name);
        const onChange = props.onChange ? props.onChange(name) : undefined;
        const obj = _.get(contactInfo, path);
        const notNullObj = obj ? obj : '';
        const value = useValue ? notNullObj : undefined;
        const defaultValue = !useValue ? notNullObj : undefined;
        return {
            ...fieldProps,
            className,
            label,
            name,
            onChange,
            value,
            defaultValue
        }
    };

    return (<Fragment>
        <TextField
            {...fieldProps({
                label: 'email',
                type: 'email'
            })}
        />
        <TextField
            {...fieldProps({
                label: 'phone',
            })}
        />
        <TextField
            {...fieldProps({
                label: 'address',
            })}
        />
        <TextField
            {...fieldProps({
                label: 'city',
            })}
        />
        <TextField
            className={classes.input}
            select={true}
            label='country'
            name='countryId'
            onChange={props.onChange && props.onChange("countryId")}
            value={!!(props.onChange && contactInfo) ? `${contactInfo.countryId}` : undefined}
            defaultValue={!!(!props.onChange && contactInfo) ? `${contactInfo.countryId}` : undefined}
            SelectProps={{
                native: true,
                MenuProps: {
                    className: classes.menu,
                },
            }}>
            {countries.map(country => {
                return (
                    <option key={country.id} value={country.id}>
                        {country.name}
                    </option>
                )
            })}
        </TextField>
        <TextField
            {...fieldProps({
                label: 'website',
            })}/>
        <TextField
            {...fieldProps({
                label: 'avatarUrl',
            })}/>
        <TextField
            {...fieldProps({
                label: 'about',
            })}/>
    </Fragment>)
};

export default withStyles(styles)(ContactInfoComponent);