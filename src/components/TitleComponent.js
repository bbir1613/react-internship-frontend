import React from 'react';
import Typography from 'material-ui/Typography';
import {withStyles} from 'material-ui/styles';

const styles = {
    root: {
    },
};

const TitleComponent = (props) => {
    const {classes, title} = props;

    return (
        <div className={classes.root}>
            <Typography variant="display3" align="center" className={classes.typography}>  {title} </Typography>
        </div>
    )
};

export default withStyles(styles)(TitleComponent);