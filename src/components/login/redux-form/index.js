import React, {Fragment} from 'react';
import {Field, reduxForm} from 'redux-form'
import {withStyles} from 'material-ui';
import {loginAction} from "../actions";
import {connect} from 'react-redux';
import Button from "material-ui/es/Button/Button";
import renderTextField from '../../../util/renderTextField';

const styles = {
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
    },
    input: {
        // top: '5px'
    },
    button: {
        alignSelf: 'center',
        marginTop: '10px',
    },
    link: {
        alignSelf: 'center',
        fontSize: '12px',
        marginTop: '5px',
    }
};

const LoginFormComponent = (props) => {
    const {classes, handleSubmit, onLoginSubmit} = props;

    return (
        <div className={classes.root}>
            <form className={classes.form} onSubmit={handleSubmit(onLoginSubmit)}>
                <Fragment>
                    <Field name="username" component={renderTextField} label='username'/>
                    <Field name="password" component={renderTextField} label='password'/>
                </Fragment>
                <Button className={classes.button} color='primary' variant='raised'
                        type="submit">Login</Button>
            </form>
        </div>
    )
};

const mapDispatchToProps = (dispatch) => ({
    onLoginSubmit: (user) => dispatch(loginAction(user))
});


const LoginReduxForm = reduxForm({form: 'login'})(connect(null, mapDispatchToProps)(withStyles(styles)(LoginFormComponent)));

export default LoginReduxForm;