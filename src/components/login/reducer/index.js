import * as LoginTypeActions from '../constants';


let reduce = {};

reduce[LoginTypeActions.LOGIN_SUCCESS] = (state, action) => ({...state, user: action.payload});
reduce[LoginTypeActions.LOGIN_FAILED] = (state, action) => ({...state, loggedIn: false});
reduce[LoginTypeActions.LOGOUT] = (state, action) => ({...initialState()});

const initialState = ()=>({
    user: {
        isLoggedIn: false
    },
});

const reducer = (state = initialState(), action) => reduce[action.type] ? reduce[action.type](state, action) : state;

export default reducer;