import React from 'react';
import {withStyles} from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Card from "material-ui/Card";
import {REGISTER_PAGE} from '../../constants/routes-name'
import {translate} from 'react-i18next';
import {loginAction} from './actions';
import getLogger from '../../util/logger';
import {connect} from 'react-redux';
import {switchRoute} from "../../pages/app/actions";

const log = getLogger("LoginComponent");
const styles = {
    root: {
        minHeight: "calc(100vh - 64px)",
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    card: {
        minWidth: '25%'
    },
    content: {
        margin: 25,
        display: 'flex',
        flexDirection: 'column'
    },
    button: {
        minWidth: '15%'
    },
    action: {
        margin: 10,
        padding: 15,
        display: 'flex',
        justifyContent: 'space-around'
    },
    toolbar: {
        display: 'flex',
        justifyContent: 'center',
        backgroundColor: '#3f51b5',
        color: 'white'
    },
};

const LoginComponent = (props) => {
    const {classes, onLoginSubmit, switchRoute, t} = props;

    const onSubmit = (event) => {
        event.preventDefault();
        const username = event.target.username.value;
        const password = event.target.password.value;
        log(`onSubmit`, username, password);
        onLoginSubmit({username, password});
    };

    return (
        <div className={classes.root}>
            <Card className={classes.card}>
                <Toolbar className={classes.toolbar}>
                    <Typography variant="title" color="inherit">
                        {t('login.LOGIN')}
                    </Typography>
                </Toolbar>
                <form className={classes.form} onSubmit={onSubmit}>
                    <div className={classes.content}>
                        <TextField className={classes.input} label='Username' name='username'/>
                        <TextField className={classes.input} label={t('login.PASSWORD')} type='password'
                                   name='password'/>
                    </div>
                    <div className={classes.action}>
                        <Button className={classes.button} color='primary' variant='raised'
                                type="submit">
                            {t('login.LOGIN')}
                        </Button>
                        <Button className={classes.button} onClick={() => switchRoute(REGISTER_PAGE)}
                                variant='raised'
                                type="button">
                            {t('login.SIGNUP')}
                        </Button>
                    </div>
                </form>
            </Card>
        </div>
    )
};

const mapDispatchToProps = (dispatch) => ({
    onLoginSubmit: (user) => dispatch(loginAction(user)),
    switchRoute: (path) => dispatch(switchRoute(path))
});

const withConnect = connect(null, mapDispatchToProps)(translate('translations')(withStyles(styles)(LoginComponent)));

export default withConnect;