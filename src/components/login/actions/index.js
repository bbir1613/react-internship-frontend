import http from '../../../configs/http';
import {USERS} from '../../../constants/api-routes';
import * as ActionType from '../constants';
import {removeUser, saveUser} from '../../../util/storage';
import getLogger from '../../../util/logger';
import {defaultPage} from '../../../constants/routes'
import {push} from 'react-router-redux'
import {LOGIN} from '../../../constants/routes-name';
import {setError, setNotification} from "../../notification/actions";
import {switchRoute} from "../../../pages/app/actions";

const log = getLogger('login/actions');

export const loginAction = (user) => dispatch => {
    log(`initLogin`, user);
    return http.post(`${USERS}/login`, user)
        .then(response => {
            log(`initLogin succes`, response.data);
            saveUser({...response.data});
            dispatch(loginSucceeded({...response.data, isLoggedIn: true}));
            dispatch(switchRoute(defaultPage(response.data)));
            dispatch(setNotification(`Login success`));
        }).catch(error => {
            log(`initLogin error`, error);
            dispatch(setError(error.response.statusText));
        });
};

export const loginSucceeded = (payload) => ({type: ActionType.LOGIN_SUCCESS, payload});

export const logoutAction = () => dispatch => {
    removeUser();
    dispatch({type: ActionType.LOGOUT});
    dispatch(push(LOGIN));
};