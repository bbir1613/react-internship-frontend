import React,{Fragment} from 'react';
import {withStyles} from 'material-ui/styles';
import HeaderComponent from './Header';
import routes from '../constants/routes';
import {Route, withRouter} from 'react-router-dom';
import ConnectedSwitch from '../configs/connectedSwitch';
import {connect} from 'react-redux';
import {logoutAction} from "./login/actions";
// import Card from 'material-ui/Card';
// import Grid from 'material-ui/Grid';


//TODO(BOGDAN) : USE GRID

const styles = {
    root:{
        minHeight:'calc(100vh - 64px)',
    }
};

const LayoutComponent = (props) => {
    const {user, logout} = props;
    return (
        <Fragment>
            <HeaderComponent user={user} logout={logout}/>
                    <ConnectedSwitch>
                        {routes.map((route, index) => {
                            if (route.show(user))
                                return <Route key={index} exact path={route.path} component={route.component}/>;
                            return null;
                        })}
                    </ConnectedSwitch>
            </Fragment>
    )
};


const mapStateToProps = (state) => ({user: state.auth.user});

const mapDispatchToProps = (dispatch) => ({
    logout: () => dispatch(logoutAction())
});

const withConnect = withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(LayoutComponent)));

export default withConnect;

/*



            <Grid container justify={'center'} style={{minHeight: 700, border: "2px solid red"}}>
                <Grid item xs={12} sm={11} md={10} lg={8}>
                    <Card style={{
                        height: 80,
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Grid container justify={"center"}>
                            <Grid item xs={12} sm={12} md={2}>
                                <Card style={{
                                    height: 80,
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}>
                                    <h1>1st grid</h1>
                                </Card>
                            </Grid>

                            <Grid item  xs={12} sm={12}  md={2}>
                                <Card style={{
                                    height: 80,
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}>
                                    <h1>1st grid</h1>
                                </Card>
                            </Grid>

                            <Grid item  xs={12} sm={6}  md={4}>
                                <Card style={{
                                    height: 80,
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}>
                                    <h1>1st grid</h1>
                                </Card>
                            </Grid>
                        </Grid>
                    </Card>
                </Grid>
            </Grid>

            <br />

            <br />



            //     </Grid>
            // </Grid>

 */