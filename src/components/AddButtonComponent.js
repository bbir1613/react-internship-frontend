import React, {Fragment} from 'react';
import Button from 'material-ui/Button';
import AddIcon from 'material-ui-icons/Add';
import {withStyles} from 'material-ui/styles';

const styles = {
    root: {},
};

const AddButtonComponent = (props) => {
    const {onClick, open} = props;
    return (
        <Fragment>
            {!open ?
                <Button variant='fab' onClick={onClick}>
                    <AddIcon/>
                </Button>
                :
                null
            }
        </Fragment>)
};

export default withStyles(styles)(AddButtonComponent);