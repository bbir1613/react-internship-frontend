import React, {Fragment} from 'react';
import {Field, FieldArray, reduxForm} from 'redux-form'
import renderTextField from '../util/renderTextField';
import Card from 'material-ui/Card';
import AddIcon from 'material-ui-icons/Add';
import Divider from "material-ui/Divider";
import {withStyles} from 'material-ui/styles';
import Button from 'material-ui/Button';
import DeleteIcon from 'material-ui-icons/Delete';

const styles = {
    root: {
        width: '50%',
        display: 'flex',
        alignSelf: 'center',
        margin: 10,
    },
    innerDiv: {
        flex: 1,
        justifyContent: "space-between"
    },
    divider: {
        width: "50%",
        alignSelf: 'center',
    },
    flex: {
        display: 'flex',
        width: '50%',
        alignSelf: 'center',
    },
    grow: {
        flex: 1,
    },

};

const ReduxListFormTest = (props) => {

    const {classes} = props;

    const handleSubmit = (huh) => {
    };

    const renderWork = ({fields}) => {
        return (
            <Fragment>
                <div className={classes.flex}>
                    <div className={classes.grow}>
                        Work
                    </div>
                    <div>
                        <Button size="small" onClick={() => {
                            fields.push({
                                id: undefined,
                                userId: undefined,
                                institution: '',
                                description: '',
                                startDate: new Date(),
                                endDate: new Date(),
                            })
                        }}><AddIcon/></Button>
                    </div>
                </div>
                <Divider className={classes.divider}/>
                {fields.map((work, index) => (
                    <Card className={classes.root} key={index}>
                        <div className={classes.innerDiv}>
                            <Field
                                label='institution'
                                name={`institution${index}`}
                                component={renderTextField}
                            />
                            <Field
                                label='description'
                                name={`description${index}`}
                                component={renderTextField}
                            />
                        </div>
                        <div>
                            <Button className={classes.button} size="small" onClick={() => {
                                fields.remove(index)
                            }}> <DeleteIcon/> </Button>
                        </div>
                    </Card>
                ))}
            </Fragment>)
    };

    return (
        <form onSubmit={props.handleSubmit(handleSubmit)}>
            <FieldArray name="works" component={renderWork}/>
        </form>);
};
export default reduxForm({
    form: 'test'
})(withStyles(styles)(ReduxListFormTest));
