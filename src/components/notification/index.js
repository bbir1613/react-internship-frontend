import React from 'react';
import Snackbar from 'material-ui/Snackbar';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui-icons/Close';
import {withStyles} from 'material-ui/styles';
import {connect} from 'react-redux';
import {clearNotification} from "./actions";

const styles = {
    root: {},
    table: {}
};

const NotificationsComponent = (props) => {
    const {classes, notification, onClose} = props;

    return (
        <div className={classes.root}>
            <Snackbar
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
                open={notification.open}
                message={`${notification.type} : ${notification.message}`}
                onClose={onClose}
                autoHideDuration={6000}
                action={[
                    <IconButton
                        key="close"
                        color={notification.type === "ERROR"? "secondary" : "inherit"}
                        onClick={onClose}
                    >
                        <CloseIcon/>
                    </IconButton>
                ]}
            />
        </div>
    )

};

const mapStateToProps = (state) => ({
    notification: state.notification
});

const mapDispatchToProps = (dispatch) => ({
    onClose: () => dispatch(clearNotification())
});

const withConnect = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(NotificationsComponent));

export default withConnect;