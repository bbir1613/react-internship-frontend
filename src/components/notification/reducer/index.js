import * as NotificationActions from '../constants';

let reduce = {};

reduce[NotificationActions.SET_NOTIFICATION] = (state, action) => ({...state, ...action.payload, open: true});
reduce[NotificationActions.CLEAR_NOTIFICATION] = (state, action) => ({...initialState()});

const initialState = ()=>({
    open: false,
    message: undefined,
    type: undefined
});

const reducer = (state = initialState(), action) => reduce[action.type] ? reduce[action.type](state, action) : state;

export default reducer;