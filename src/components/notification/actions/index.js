import * as NOTIFICATION from "../../../constants/notifications";
import {SET_NOTIFICATION, CLEAR_NOTIFICATION} from '../constants';

export const setNotification = (message) => notification({message, type: NOTIFICATION.NOTIFICATION});
export const setError = (message) => notification({message, type: NOTIFICATION.ERROR});
export const clearNotification = () => ({type: CLEAR_NOTIFICATION});

const notification = (payload) => ({type: SET_NOTIFICATION, payload});