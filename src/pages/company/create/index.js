import React, {Fragment} from 'react';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import Dialog, {
    DialogActions,
    DialogContent,
    DialogTitle,
} from 'material-ui/Dialog';
import {withStyles} from 'material-ui/styles';
import ContactInfoComponent from '../../../components/ContactInfoComponent';
// import getLogger from '../../../util/logger';

// const log = getLogger("CompanyCreateOrEditComponent");

const styles = {
    root: {
        // width: '20%',
        // marginLeft: '5%'
    },
    dialog: {
        // display: 'flex',
        // flexDirection: 'column',
    },
    content: {
        display: 'flex',
        // alignSelf: 'center',
        flexDirection: 'column',
        // width: "50%",
    },
    form: {
        // display: 'flex',
        // flexDirection: 'column',
        // width: "50%",
        // alignSelf: "center"
        // justifyContent: 'center',
    },
    title: {
        display: 'flex',
        // width: '50%',
        alignSelf: 'center'
    },
    input: {
        margin: 10,
    },
    paperClass: {
        width: '50%',
        maxWidth: '100%',
        // opacity: '0.3 !important',
    }
};


const CompanyCreateOrEditComponent = (props) => {
    const {classes, onCreateCompanySubmit, onClose, company, countries, open} = props;

    const onSubmit = (event) => {
        event.preventDefault();
        const name = event.target.name.value;
        const email = event.target.email.value;
        const phone = event.target.phone.value;
        const city = event.target.city.value;
        const countryId = event.target.countryId.value;
        const address = event.target.address.value;
        const website = event.target.website.value;
        const avatarUrl = event.target.avatarUrl.value;
        const about = event.target.about.value;

        const contactInfo = {
            id: company && company.contactInfo ? company.contactInfo.id : undefined,
            email,
            phone,
            city,
            countryId,
            address,
            website,
            avatarUrl,
            about
        };
        onCreateCompanySubmit({...company, name, contactInfo});
    };

    return (
        <Dialog
            fullScreen={false}
            className={classes.dialog}
            open={open}
            classes={{
                paper: classes.paperClass
            }}
            onClose={onClose}
        >
            <DialogTitle className={classes.title}>{company ? 'Edit company' : 'Create company'}</DialogTitle>
            <form
                className={classes.form}
                onSubmit={onSubmit}>
                <DialogContent className={classes.content}>
                    <Fragment>
                        <TextField className={classes.input} label='name' name='name'
                                   defaultValue={company ? company.name : ''}/>
                        <ContactInfoComponent
                            countries={countries}
                            contactInfo={company && company.contactInfo ? company.contactInfo : undefined}
                        />
                    </Fragment>
                </DialogContent>
                <DialogActions>
                    <Button onClick={onClose} type="button" color="primary">
                        Cancel
                    </Button>
                    <Button color="primary" type="submit">
                        {company ? 'Edit' : 'Create'}
                    </Button>
                </DialogActions>
            </form>
        </Dialog>
    )
};

export default withStyles(styles)(CompanyCreateOrEditComponent);