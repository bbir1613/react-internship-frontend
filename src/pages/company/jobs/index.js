import React, {Component} from 'react';
import JobCreateComponent from '../../job/create/index';
import JobListComponent from '../../job/list/index';
import ConfirmJobDeleteDialogComponent from '../../../components/ConfirmDialogComponent';
import ConfirmDeleteDialogComponent from '../../../components/ConfirmDialogComponent';
import TitlePageComponent from '../../../components/TitlePageComponent';
import {withStyles} from 'material-ui/styles';
import {connect} from 'react-redux';
import getLogger from '../../../util/logger';
import {getCompaniesByUserId} from "../actions";
import * as JobActions from '../../job/actions';
import {resetAction} from "../../app/actions";
import _ from 'lodash';
import {removeFromObject} from "../../../util";


const log = getLogger("CompanyJobPage", false);

const styles = {
    root: {},
    content: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    }
};

const getInitialState = () => ({
    selectedJob: undefined,
    openJobCreateComponent: false,
    openJobDialogDeleteComponent: false,
    openDeleteDialogComponent: false,
    pathToSelectedItemToDelete: undefined,
    jobDetails: {
        requirements: [],
        benefits: [],
        skills: []
    }
});

class CompanyJobsPage extends Component {

    state = getInitialState();

    getData = () => {
        const companyId = this.props.match.params.id;
        if (!!companyId) {
            this.props.getJobsByCompanyId(companyId);
        } else {
            this.props.getCompaniesByUserId(this.props.user.id);
            this.props.getAllJobsByUserId(this.props.user.id);
        }
    };

    addProp = (...propName) => (prop) => () => {
        const newState = {...this.state};
        const path = propName.concat(prop);
        let obj = undefined;
        if (prop === "skills") {
            obj = {
                id: undefined,
                rating: 0,
                skillId: this.props.availableSkills[0].id,
                jobId: this.state.selectedJob ? this.state.selectedJob.id : undefined
            };
        } else {
            obj = {
                id: undefined,
                name: '',
                jobId: this.state.selectedJob ? this.state.selectedJob.id : undefined
            };
        }
        const value = _.get(newState, path).concat(obj);
        _.set(newState, path, value);
        this.setState(newState);
    };

    onRemove = (...propName) => (index) => () => {
        log(` onRemove`, propName, index);
        const pathToSelectedItemToDelete = propName.concat(index);
        this.setState({...this.state, pathToSelectedItemToDelete, openDeleteDialogComponent: true})
    };

    onValueChange = (...propName) => index => prop => (event) => {
        log(`onValueChange `, propName, index, ` prop `, event.target.value);
        const newState = {...this.state};
        const value = event.target.value;
        const path = _.without(propName.concat(index, prop), null, undefined);
        _.set(newState, path, value);
        this.setState(newState);
    };

    onAddButton = () => {
        log(`onAddButton`);
        this.setState(prevState => {
            return {openJobCreateComponent: !prevState.openJobCreateComponent, selectedJob: undefined};
        });
    };

    onCloseCreateComponent = () => {
        log(`onCloseCreateComponent`);
        this.setState(getInitialState());
    };

    onSelectJob = (job) => {
        log(`onSelectJob`, job);
        this.props.getJobInfoById(job.id).then(jobInfo => {
            this.setState({
                openJobCreateComponent: true,
                selectedJob: {...job},
                jobDetails: {
                    requirements: [...jobInfo.jobRequirementInfoList],
                    benefits: [...jobInfo.jobBenefitInfoList],
                    skills: [...jobInfo.jobSkillInfoList],
                }
            });
        })
    };

    onDeleteJobRequest = (job) => {
        log(`onDeleteJobRequest`);
        this.setState({openJobDialogDeleteComponent: true, selectedJob: {...job}});
    };

    onJobDialogDeleteComponentAgree = () => {
        log(`onCompanyDialogDeleteComponentAgree`);
        const job = {...this.state.selectedJob};
        job.isAvailable = false;
        this.props.updateJob(job).then(() => {
            this.setState(getInitialState());
            this.getData();
        });
    };

    onJobDialogDeleteComponentDisagree = () => {
        log(`onJobDialogDeleteComponentDisagree`);
        this.setState(getInitialState());
    };

    onCreateJobSubmit = (job) => {
        log(`onCreateJobSubmit`);
        const callback = () => {
            this.setState(getInitialState());
            this.getData();
        };
        if (job.id) {
            this.props.updateJob(job, {...this.state.jobDetails}).then(callback);
        } else {
            this.props.addJob(job, {...this.state.jobDetails}).then(callback);
        }
    };

    onConfirmDeleteDialogComponentAgree = async () => {
        log(` onConfirmDeleteDialogComponentAgree `, this.state.pathToSelectedItemToDelete);
        const request = {
            'requirements': this.props.removeRequirement,
            'benefits': this.props.removeBenefit,
            'skills': this.props.removeSkill
        };
        const newState = await removeFromObject({...this.state}, [...this.state.pathToSelectedItemToDelete], request);
        this.setState(newState, this.onConfirmDeleteDialogComponentDisagree);
    };


    onConfirmDeleteDialogComponentDisagree = () => {
        log(` onConfirmDeleteDialogComponentDisagree `);
        this.setState({...this.state, pathToSelectedItemToDelete: '', openDeleteDialogComponent: false})
    };


    render() {
        const {classes} = this.props;

        return (
            <div className={classes.root}>
                <TitlePageComponent title="Job page" subtitle="Job" open={this.state.openUserCreateComponent}
                                    onClick={this.onAddButton}/>
                <div className={classes.content}>
                    <JobCreateComponent onCreateJobSubmit={this.onCreateJobSubmit}
                                        companies={this.props.companies}
                                        job={this.state.selectedJob}
                                        open={this.state.openJobCreateComponent}
                                        onClose={this.onCloseCreateComponent}
                                        jobDetails={this.state.jobDetails}
                                        onRequirementChange={this.onValueChange("jobDetails", "requirements")}
                                        onBenefitChange={this.onValueChange("jobDetails", "benefits")}
                                        onSkillChange={this.onValueChange("jobDetails", "skills")}
                                        onRequirementRemove={this.onRemove("jobDetails", "requirements")}
                                        onBenefitRemove={this.onRemove("jobDetails", "benefits")}
                                        onSkillRemove={this.onRemove("jobDetails", "skills")}
                                        addRequirement={this.addProp('jobDetails')('requirements')}
                                        addBenefits={this.addProp('jobDetails')('benefits')}
                                        addSkills={this.addProp('jobDetails')('skills')}
                                        availableSkills={this.props.availableSkills}
                    />
                    <JobListComponent jobs={this.props.jobs}
                                      onEditJob={this.onSelectJob}
                                      onDeleteJob={this.onDeleteJobRequest}
                    />
                </div>
                <ConfirmJobDeleteDialogComponent open={this.state.openJobDialogDeleteComponent}
                                                 title={"Are you sure you want to delete the following job"}
                                                 info={this.state.selectedJob ? this.state.selectedJob.name : null}
                                                 onAgree={this.onJobDialogDeleteComponentAgree}
                                                 onDisagree={this.onJobDialogDeleteComponentDisagree}
                />
                <ConfirmDeleteDialogComponent open={this.state.openDeleteDialogComponent}
                                              title={"Are you sure you want to delete ?"}
                                              onAgree={this.onConfirmDeleteDialogComponentAgree}
                                              onDisagree={this.onConfirmDeleteDialogComponentDisagree}
                />
            </div>
        )
    }

    componentDidMount() {
        log(`componentDidMount`);
        this.getData();
    }

    componentWillUnmount() {
        log(`componentWillUnmount`);
        this.props.resetAction();
    }
}

const mapStateToProps = (state) => ({
    companies: state.company.companies,
    jobs: state.job.jobs,
    user: state.auth.user,
    availableSkills: state.app.skills,
});

const mapDispatchToProps = (dispatch) => ({
    getCompaniesByUserId: (userId) => dispatch(getCompaniesByUserId(userId)),
    getAllJobsByUserId: (userId) => dispatch(JobActions.getAllJobsByUserId(userId)),
    getJobsByCompanyId: (companyId) => dispatch(JobActions.getJobsByCompanyId(companyId)),
    getJobInfoById: (jobId) => dispatch(JobActions.getJobInfoById(jobId)),
    addJob: (job, jobDetails) => dispatch(JobActions.addJob(job, jobDetails)),
    updateJob: (job, jobDetails) => dispatch(JobActions.updateJob(job, jobDetails)),
    removeSkill: (id) => dispatch(JobActions.removeSkill(id)),
    removeBenefit: (id) => dispatch(JobActions.removeBenefit(id)),
    removeRequirement: (id) => dispatch(JobActions.removeRequirement(id)),
    resetAction: () => dispatch(resetAction()),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(CompanyJobsPage));

export default withConnect;