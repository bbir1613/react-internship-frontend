import React, {Component} from 'react';
import {withStyles} from 'material-ui/styles';
import Divider from 'material-ui/Divider';
import UserAppliedForJobListComponent from '../../list/userapplied';
import ConfirmAcceptDialogComponent from '../../../../components/ConfirmDialogComponent';
import TitleComponent from '../../../../components/TitleComponent';
import getLogger from '../../../../util/logger';
import {getUsersThatAppliedForAJob} from "../../../user/actions";
import {acceptUserToJob} from "../../../job/actions";
import {resetAction} from "../../../app/actions";
import {connect} from 'react-redux';

const log = getLogger("CompanyUserAppliedForJobDetailsPage");

const styles = {
    root: {},
    content: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    }
};

class CompanyUserAppliedForJobDetailsPage extends Component {

    state = {
        openDialogConfirmComponent: false,
        selectedUser: undefined
    };

    selectUser = (user) => {
        log(`selectUser`, user);
        this.setState({selectedUser: {...user}, openDialogConfirmComponent: true});
    };

    onUserDialogConfirmComponentAgree = () => {
        log(`onUserDialogConfirmComponentAgree`);
        const user = {...this.state.selectedUser, isAccepted: true};
        this.props.acceptUserToJob(user).then(() => {
            this.props.getUsersThatAppliedForAJob(this.props.match.params.id);
            this.setState({selectedUser: undefined, openDialogConfirmComponent: false});
        });
    };

    onUserDialogConfirmComponentDisagree = () => {
        log(`onUserDialogConfirmComponentDisagree`);
        this.setState({selectedUser: undefined, openDialogConfirmComponent: false});
    };

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <TitleComponent title="User that applied for job page"/>
                <Divider/>
                <div className={classes.content}>
                    <UserAppliedForJobListComponent
                        users={this.props.users}
                        onAccept={this.selectUser}
                    />
                </div>
                <ConfirmAcceptDialogComponent open={this.state.openDialogConfirmComponent}
                                              onAgree={this.onUserDialogConfirmComponentAgree}
                                              onDisagree={this.onUserDialogConfirmComponentDisagree}
                                              title="Are you sure you want to accept current candidate"
                                              info={this.state.selectedUser ? this.state.selectedUser.userInfo.username : ''}
                />
            </div>
        )
    }

    componentDidMount() {
        log(`componentDidMount`);
        this.props.getUsersThatAppliedForAJob(this.props.match.params.id);
    }

    componentWillUnmount() {
        log(`componentWillUnmount`);
        this.props.resetAction();
    }
}

const mapStateToProps = (state) => ({
    users: state.user.users,
});

const mapDispatchToProps = (dispatch) => ({
    getUsersThatAppliedForAJob: (jobId) => dispatch(getUsersThatAppliedForAJob(jobId)),
    acceptUserToJob: (user) => dispatch(acceptUserToJob(user)),
    resetAction: () => dispatch(resetAction),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(CompanyUserAppliedForJobDetailsPage));

export default withConnect;