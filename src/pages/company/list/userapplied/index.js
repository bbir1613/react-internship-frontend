import React, {Fragment} from 'react';
import {withRouter} from 'react-router-dom';
import Table, {TableBody, TableCell, TableHead, TableRow} from 'material-ui/Table';
import Typography from 'material-ui/Typography';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper'
import Button from 'material-ui/Button';
import {withStyles} from 'material-ui/styles';

const styles = {
    root: {
        marginTop: 10,
        marginBottom: 10,
    },
    table: {},
    title: {
        margin: 10
    }
};

const UserAppliedForJobListComponent = (props) => {
    const {classes, history, users, onAccept} = props;
    const redirect = (userId) => history.push(`/user/profile/${userId}`);
    return (
        <Paper className={classes.root}>
            {users.length ?
                <Fragment>
                    <Typography variant="display2" className={classes.title}>
                        Users
                    </Typography>
                    <Divider/>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell>Username</TableCell>
                                <TableCell>FirstName</TableCell>
                                <TableCell>LastName</TableCell>
                                <TableCell>Accept</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {users.map((user, index) => {
                                return (
                                    <TableRow key={index}
                                              hover={true}
                                    >
                                        <TableCell
                                            size="small" onClick={() => redirect(user.userInfo.id)}
                                        >
                                            {user.userInfo.username}
                                        </TableCell>
                                        <TableCell
                                            size="small" onClick={() => redirect(user.userInfo.id)}
                                        > {user.userInfo.firstName}</TableCell>
                                        <TableCell
                                            size="small" onClick={() => redirect(user.userInfo.id)}
                                        > {user.userInfo.lastName}</TableCell>
                                        <TableCell><Button size="small"
                                                           onClick={() => onAccept(user)}>Accept</Button></TableCell>
                                    </TableRow>
                                )
                            })}
                        </TableBody>
                    </Table>
                </Fragment>
                :
                null
            }
        </Paper>
    )
};

export default withStyles(styles)(withRouter(UserAppliedForJobListComponent));