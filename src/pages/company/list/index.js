import React, {Fragment} from 'react';
import Table, {TableBody, TableCell, TableHead, TableRow} from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import DeleteIcon from 'material-ui-icons/Delete';
import EditIcon from 'material-ui-icons/Edit';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import Divider from 'material-ui/Divider';
import {withRouter} from 'react-router-dom';
import {withStyles} from 'material-ui/styles';

const styles = {
    root: {
        marginTop: 10,
        marginBottom: 10,
    },
    table: {},
    title: {
        margin: 10
    }
};

const redirect = (history, id, isUser) => {
    if (isUser)
        history.push(`company/${id}`);
    else
        history.push(`company/jobs/${id}`);
};

const CompanyListComponent = (props) => {
    const {classes, companies, onEditCompany, onDeleteCompany, history, isUser = false} = props;
    return (
        <Paper className={classes.root}>
            {companies.length ?
                <Fragment>
                    <Typography variant="display2" className={classes.title}>
                        Companies
                    </Typography>
                    <Divider/>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell>Name</TableCell>
                                {!isUser ?
                                    <TableCell/>
                                    :
                                    null
                                }
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {companies.map((company, index) => {
                                return (
                                    <TableRow key={index} hover={true}>
                                        <TableCell
                                            onClick={() => redirect(history, company.id, isUser)}> {company.name} </TableCell>
                                        {!isUser ?
                                            <TableCell>
                                                <Button size="small"
                                                        onClick={() => onEditCompany(company)}><EditIcon/></Button>
                                                <Button size="small"
                                                        onClick={() => onDeleteCompany(company)}><DeleteIcon/></Button>
                                            </TableCell>
                                            : null}
                                    </TableRow>
                                )
                            })}
                        </TableBody>
                    </Table>
                </Fragment>
                :
                null
            }
        </Paper>
    )
};

export default withStyles(styles)(withRouter(CompanyListComponent));