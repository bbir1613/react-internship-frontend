import React from 'react';
import {withStyles} from 'material-ui/styles';
import {translate} from 'react-i18next';
import {withRouter} from 'react-router-dom';
import Card, {CardActions, CardContent} from 'material-ui/Card';
import Button from 'material-ui/Button';

//import getLogger from '../util/logger';
//const log = getLogger("CompanyCardComponent");

const styles = {
    root: {
        padding: 25,
        width:'30%',
        display:'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    card:{
        width: '100%',
        textAlign: 'center',
        margin : 15,
    }
};

const redirect = (history, id) => {
    history.push(`company/${id}`);
};


const CompanyCardComponent = (props) => {
    const {classes, companies, history} = props;
    return (
        <div className={classes.root}>
            {companies.length ?
                companies.map((company, index) => {
                    return (
                        <Card className={classes.card} key={index}>
                            <CardContent>
                                {company.name}
                            </CardContent>
                            <CardActions>
                                <Button size="small" onClick={() => redirect(history, company.id)}>Jobs</Button>
                            </CardActions>
                        </Card>
                    );
                })
                :
                null}
        </div>)
};

export default translate('translations')(withStyles(styles)(withRouter(CompanyCardComponent)));