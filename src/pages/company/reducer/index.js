import * as CompanyTypeActions from '../constants';
import {RESET_ACTION} from "../../app/constants";

let reduce = {};

reduce[CompanyTypeActions.GET_COMPANIES_SUCCEEDED] = (state, action) => ({...state, companies: action.payload});
reduce[RESET_ACTION] = (state, action) => (initialState());

const initialState = ()=>({
    companies: []
});

const reducer = (state = initialState(), action) => reduce[action.type] ? reduce[action.type](state, action) : state;

export default reducer;