import http from "../../../configs/http";
import {COMPANIES} from "../../../constants/api-routes";
import getLogger from '../../../util/logger';
import * as CompanyActionType from '../constants';
import {setNotification, setError} from '../../../components/notification/actions';

const log = getLogger('company-actions');

export const getAllCompanies = () => dispatch => {
    log(`getAllCompanies `);
    return http.get(`${COMPANIES}`)
        .then(response => {
            log(`getAllCompanies  success`, response.data);
            dispatch(getCompaniesSucceeded(response.data));
        }).catch(error => {
            log(`getAllCompanies  error`, error);
            dispatch(setError(error.response.statusText));
        });

};

export const getCompaniesByUserId = (id) => dispatch => {
    log(`getCompaniesByUserId`);
    return http.get(`${COMPANIES}/user/${id}`)
        .then(response => {
            log(`getCompaniesByUserId success`, response.data);
            dispatch(getCompaniesSucceeded(response.data));
        }).catch(error => {
            log(`getCompaniesByUserId error`, error);
            dispatch(setError(error.response.statusText));
        });
};

export const addCompany = (company) => dispatch => {
    log(`addCompany`, company);
    return http.post(`${COMPANIES}`, company)
        .then(response => {
            log(`addCompany success`, response.data);
            dispatch(setNotification(`Company added with success`));
            // dispatch(getCompaniesByUserId(userId));
        }).catch(error => {
            log(`addCompany error`, error);
            dispatch(setError(error.response.statusText));
        });
};

export const updateCompany = (company) => dispatch => {
    log(`updateCompany`, company);
    return http.put(`/contacts/${company.contactInfo.id}`, company.contactInfo).then(resp => {
        delete company.contactInfo;
        return http.put(`${COMPANIES}/${company.id}`, company)
            .then(response => {
                log(`updateCompany success`, response.data);
                dispatch(setNotification(`Company updated with success`));
            })
    }).catch(error => {
        log(`updateCompany error`, error);
        dispatch(setError(error.response.statusText));
    });
};

export const deleteCompany = (companyId) => dispatch => {
    log(`deleteCompany`);
    return http.delete(`${COMPANIES}/${companyId}`)
        .then(response => {
            log(`deleteCompany succes`, response.data);
            dispatch(setNotification(`Company deleted with success`));
        }).catch(error => {
            log(`deleteCompany error`, error);
            dispatch(setError(error.response.statusText));
        })
};

export const getCompaniesSucceeded = (payload) => ({type: CompanyActionType.GET_COMPANIES_SUCCEEDED, payload});