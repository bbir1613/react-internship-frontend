import React, {Component} from 'react';
import CompanyCreateOrEditComponent from './create/index';
import CompanyListComponent from './list';
import TitlePageComponent from '../../components/TitlePageComponent';
import ConfirmDeleteDialogComponent from '../../components/ConfirmDialogComponent';
import {withStyles} from 'material-ui/styles';
import {connect} from 'react-redux';
import * as CompanyActions from './actions';
import getLogger from '../../util/logger';
import {resetAction} from "../app/actions";

const log = getLogger("CompanyPage", false);

const styles = {
    root: {},
    content: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    }
};

class CompanyPage extends Component {

    state = {
        openCompanyCreateComponent: false,
        openJobDialogDeleteComponent: false,
        selectedCompany: undefined
    };

    onCreateCompanySubmit = (company) => {
        log(`onCreateCompanySubmit`);
        company.userId = this.props.user.id;
        if (company.id) {
            this.props.updateCompany(company).then(() => {
                this.props.getCompaniesByUserId(this.props.user.id);
                this.setState({openCompanyCreateComponent: false, selectedCompany: undefined});
            });
        } else {
            this.props.addCompany(company).then(() => {
                this.props.getCompaniesByUserId(this.props.user.id);
                this.setState({openCompanyCreateComponent: false});
            });
        }
    };

    onAddButton = () => {
        log(`onAddButton`);
        this.setState(prevState => {
            return {openCompanyCreateComponent: !prevState.openCompanyCreateComponent, selectedCompany: undefined};
        });
    };

    onSelectCompany = (company) => {
        log(`onSelectCompany`, company);
        this.setState({
            openCompanyCreateComponent: true,
            selectedCompany: {...company}
        });
    };

    onCompanyDialogDeleteComponentAgree = () => {
        log(`onCompanyDialogDeleteComponentAgree`);
        this.props.deleteCompany(this.state.selectedCompany.id).then(() => {
            this.props.getCompaniesByUserId(this.props.user.id);
            this.setState({openJobDialogDeleteComponent: false, selectedCompany: undefined});
        });
    };

    onCompanyDialogDeleteComponentDisagree = () => {
        log(`onCompanyDialogDeleteComponentDisagree`);
        this.setState({openJobDialogDeleteComponent: false, selectedCompany: undefined});
    };

    onDeleteCompanyRequest = (company) => {
        log(`onDeleteCompanyRequest`, company);
        this.setState({openJobDialogDeleteComponent: true, selectedCompany: {...company}});
    };

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <TitlePageComponent title="Company page" subtitle="Company" open={this.state.openUserCreateComponent}
                                    onClick={this.onAddButton}/>
                <div className={classes.content}>
                    <CompanyCreateOrEditComponent onCreateCompanySubmit={this.onCreateCompanySubmit}
                                                  company={this.state.selectedCompany}
                                                  countries={this.props.countries}
                                                  open={this.state.openCompanyCreateComponent}
                                                  onClose={this.onAddButton}
                    />
                    <CompanyListComponent companies={this.props.companies}
                                          onEditCompany={this.onSelectCompany}
                                          onDeleteCompany={this.onDeleteCompanyRequest}
                    />
                </div>
                <ConfirmDeleteDialogComponent open={this.state.openJobDialogDeleteComponent}
                                              title={"Are you sure you want to delete the following company"}
                                              info={this.state.selectedCompany ? this.state.selectedCompany.name : null}
                                              onAgree={this.onCompanyDialogDeleteComponentAgree}
                                              onDisagree={this.onCompanyDialogDeleteComponentDisagree}
                />
            </div>
        )
    }

    componentDidMount() {
        log(`componentDidMount`);
        this.props.getCompaniesByUserId(this.props.user.id);
    }

    componentWillUnmount() {
        log(`componentWillUnmount`);
        this.props.resetAction();
    }
}

const mapStateToProps = (state) => ({
    companies: state.company.companies,
    countries: state.app.countries,
    user: state.auth.user
});

const mapDispatchToProps = (dispatch) => ({
    getCompaniesByUserId: (userId) => dispatch(CompanyActions.getCompaniesByUserId(userId)),
    addCompany: (company) => dispatch(CompanyActions.addCompany(company)),
    updateCompany: (company) => dispatch(CompanyActions.updateCompany(company)),
    deleteCompany: (companyId) => dispatch(CompanyActions.deleteCompany(companyId)),
    resetAction: () => dispatch(resetAction()),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(CompanyPage));

export default withConnect;