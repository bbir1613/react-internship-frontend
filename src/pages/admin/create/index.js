import React, {Fragment} from 'react';
import Paper from 'material-ui/Paper';
import {withStyles} from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
// import getLogger from '../../../util/logger';

// const log = getLogger("SkillCreateComponent");

const styles = {
    root: {},
    paper: {
        marginTop: 10,
        marginBottom: 10,
        display: 'flex',
        minWidth: '20%'
    },
    form: {
        width: '100%'
    },
    input: {},
    content: {
        display: 'flex',
        flexDirection: 'column',
        margin: 10,
    },
    action: {
        margin: 10,
        display: 'flex',
        flexDirection: 'row-reverse'
    },
    button: {
        margin: 5
    },
};

const SkillCreateComponent = (props) => {
    const {classes, onCreateSkillSubmit, open, skill, onCancel} = props;

    const onSubmit = (event) => {
        event.preventDefault();
        onCreateSkillSubmit({id: skill ? skill.id : undefined, name: event.target.name.value});
    };

    return (
        <Fragment>
            {open ?
                <Paper className={classes.paper}>
                    <form className={classes.form} onSubmit={onSubmit}>
                        <div className={classes.content}>
                            <TextField className={classes.input} label='name' name='name'
                                       defaultValue={skill ? skill.name : ''}/>
                        </div>
                        <div className={classes.action}>
                            <Button className={classes.button}
                                    color='primary'
                                    variant='raised'
                                    type="submit">
                                {skill ? 'Edit' : 'Create'}
                            </Button>
                            <Button className={classes.button}
                                    color='primary'
                                    variant='raised'
                                    onClick={onCancel}
                                    type="button">
                                {'Cancel'}
                            </Button>
                        </div>
                    </form>
                </Paper>
                :
                null}
        </Fragment>
    )
};

export default withStyles(styles)(SkillCreateComponent);