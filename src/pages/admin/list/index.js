import React, {Fragment} from 'react';
import List, {ListItem, ListItemSecondaryAction, ListItemText,} from 'material-ui/List';
import Paper from 'material-ui/Paper';
import DeleteIcon from 'material-ui-icons/Delete';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui-icons/Edit';
import Typography from 'material-ui/Typography';
import Divider from 'material-ui/Divider';
import {withStyles} from 'material-ui/styles';

const styles = {
    root: {
        marginTop: 10,
        marginBottom: 10,
    },
    table: {},
    title: {
        margin: 10
    }
};

const SkillListComponent = (props) => {
    const {classes, skills, onSkillDelete, onSkillEdit} = props;
    return (
        <Paper className={classes.root}>
            {skills.length ?
                <Fragment>
                    <Typography variant="display2" className={classes.title}>
                        Skills
                    </Typography>
                    <Divider/>
                    <List style={{minWidth: 200}}>
                        {skills.map((skill, index) => (
                            <ListItem key={index}>
                                <ListItemText
                                    primary={skill.name}
                                />
                                <ListItemSecondaryAction>
                                    <IconButton onClick={() => onSkillDelete(skill)}>
                                        <DeleteIcon/>
                                    </IconButton>
                                    <IconButton onClick={() => onSkillEdit(skill)}>
                                        <EditIcon/>
                                    </IconButton>
                                </ListItemSecondaryAction>
                            </ListItem>)
                        )}
                    </List>
                </Fragment>
                :
                null
            }
        </Paper>
    )
};

export default withStyles(styles)(SkillListComponent);