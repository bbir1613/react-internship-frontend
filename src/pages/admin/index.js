import React, {Component} from 'react';
import {withStyles} from 'material-ui/styles';
import UserCreateComponent from '../user/create';
import UserListComponent from '../user/list';
import ConfirmDeleteDialogComponent from '../../components/ConfirmDialogComponent';
import {connect} from 'react-redux';
import getLogger from '../../util/logger';
import * as UserActions from "../user/actions";
import TitlePageComponent from '../../components/TitlePageComponent';
import {resetAction} from "../app/actions";

const log = getLogger("SkillPage");

const styles = {
    root: {},
    content: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    }
};

class AdminPage extends Component {

    state = {
        openUserCreateComponent: false,
        openJobDialogDeleteComponent: false,
        selectedUser: undefined
    };

    onAddUserSubmit = (user) => {
        log(`onAddUserSubmit`, user);
        if (user.id) {
            this.props.updateUser(user).then(() => {
                this.props.getUsers();
                this.setState({
                    openUserCreateComponent: false,
                    selectedUser: undefined
                });
            });
        } else {
            this.props.addUser(user).then(() => {
                this.props.getUsers();
                this.setState({
                    openUserCreateComponent: false,
                    selectedUser: undefined
                });
            });
        }
    };

    onAddButton = () => {
        log(`onAddButton`);
        this.setState(prevState => {
            return {openUserCreateComponent: !prevState.openUserCreateComponent, selectedUser: undefined};
        });
    };

    onDeleteUserRequest = (user) => {
        log(`onDeleteUserRequest`, user);
        this.setState({openJobDialogDeleteComponent: true, selectedUser: {...user}});
    };

    onUserDialogDeleteComponentDisagree = () => {
        log(`onUserDialogDeleteComponentDisagree`);
        this.setState({openJobDialogDeleteComponent: false, selectedUser: undefined});
    };

    onUserDialogDeleteComponentAgree = () => {
        log(`onUserDialogDeleteComponentAgree`);
        this.props.deleteUser(this.state.selectedUser.id).then(() => {
            this.props.getUsers();
            this.setState({openJobDialogDeleteComponent: false, selectedUser: undefined});
        });
    };

    onSelectUser = (user) => {
        log(`onSelectUser`);
        if (this.state.openUserCreateComponent) {
            log(`onSelectUser`, `the create component is already open `, this.state.openUserCreateComponent);
            this.setState({selectedUser: undefined, openUserCreateComponent: false}, () => {
                log(`onSelectUser`, `after we set the current select user to undefined and open component to false`, this.state.selectedUser, this.state.openUserCreateComponent);
                this.setState({selectedUser: {...user}, openUserCreateComponent: true})
            });
        } else {
            log(`onSelectUser`, `the create component is not open `, this.state.openUserCreateComponent);
            this.setState({selectedUser: {...user}, openUserCreateComponent: true});
        }
    };

    render() {
        const {classes, roles} = this.props;
        return (
            <div className={classes.root}>
                <TitlePageComponent title="User page" subtitle="User" open={this.state.openUserCreateComponent}
                                    onClick={this.onAddButton}/>
                <div className={classes.content}>
                    <UserCreateComponent onCreateUserSubmit={this.onAddUserSubmit}
                                         roles={roles}
                                         open={this.state.openUserCreateComponent}
                                         onCancel={this.onAddButton}
                                         user={this.state.selectedUser}
                    />
                    <UserListComponent users={this.props.users}
                                       roles={roles}
                                       onDeleteUser={this.onDeleteUserRequest}
                                       onEditUser={this.onSelectUser}
                    />
                </div>
                <ConfirmDeleteDialogComponent open={this.state.openJobDialogDeleteComponent}
                                              title={"Are you sure you want to delete the following user"}
                                              info={this.state.selectedUser ? this.state.selectedUser.username : null}
                                              onAgree={this.onUserDialogDeleteComponentAgree}
                                              onDisagree={this.onUserDialogDeleteComponentDisagree}
                />
            </div>
        )
    }

    componentDidMount() {
        log(`componentDidMount`);
        this.props.getUsers();
    }

    componentWillUnmount() {
        log(`componentWillUnmount`);
        this.props.resetAction();
    }
}

const mapStateToProps = (state) => ({
    roles: state.app.roles,
    users: state.user.users,
});

const mapDispatchToProps = (dispatch) => ({
    getUsers: () => dispatch(UserActions.getUsers()),
    addUser: (user) => dispatch(UserActions.addUser(user)),
    updateUser: (user) => dispatch(UserActions.updateUser(user)),
    deleteUser: (userId) => dispatch(UserActions.deleteUser(userId)),
    resetAction: () => dispatch(resetAction()),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(AdminPage));

export default withConnect;


/*

            <div className={classes.root}>


            </div>


 */