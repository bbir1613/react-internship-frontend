import React, {Component} from 'react';
import {withStyles} from 'material-ui/styles';
import {connect} from 'react-redux';
import getLogger from '../../../util/logger';
import SkillListComponent from '../list';
import SkillCreateComponent from '../create';
import ConfirmDeleteDialogComponent from '../../../components/ConfirmDialogComponent';
import TitlePageComponent from '../../../components/TitlePageComponent'
import {addSkill, deleteSkill, resetAction} from '../../app/actions'

const log = getLogger("SkillPage", false);

const styles = {
    root: {},
    content: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    }

};

class SkillPage extends Component {

    state = {
        openJobDialogDeleteComponent: false,
        openSkillCreateComponent: false,
        selectedSkill: undefined,
    };

    onDeleteSkillRequest = (skill) => {
        log(`onDeleteSkillRequest`, skill);
        this.setState({openJobDialogDeleteComponent: true, selectedSkill: {...skill}});
    };

    onSkillDialogDeleteComponentDisagree = () => {
        log(`onSkillDialogDeleteComponentDisagree`);
        this.setState({openJobDialogDeleteComponent: false, selectedSkill: undefined});
    };

    onSkillDialogDeleteComponentAgree = () => {
        log(`onSkillDialogDeleteComponentAgree`);
        this.props.deleteSkill(this.state.selectedSkill.id).then(() => {
            this.setState({openJobDialogDeleteComponent: false, selectedSkill: undefined});
        });
    };

    onSelectSkill = (skill) => {
        log(`onSelectSkill`, skill);
        if (this.state.openSkillCreateComponent) {
            this.setState({
                    ...this.state,
                    openSkillCreateComponent: false,
                    selectedSkill: undefined
                },
                () => {
                    this.setState({
                        ...this.state,
                        openSkillCreateComponent: true,
                        selectedSkill: {...skill}
                    });
                });
        } else {
            this.setState({
                ...this.state,
                openSkillCreateComponent: true,
                selectedSkill: {...skill}
            });
        }
    };

    onAddSkillSubmit = (skill) => {
        log(`onAddSkillSubmit`, skill);
        this.props.addSkill(skill).then(() => this.setState({
            ...this.state,
            openSkillCreateComponent: false,
            selectedSkill: undefined
        }));
    };

    onAddButton = () => {
        log(`onAddButton`);
        this.setState(prevState => {
            return {openSkillCreateComponent: !prevState.openSkillCreateComponent, selectedSkill: undefined};
        });
    };

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <TitlePageComponent
                    title="Skill Page"
                    subtitle="Skill"
                    open={this.state.openSkillCreateComponent}
                    onClick={this.onAddButton}/>
                <div className={classes.content}>
                    <SkillCreateComponent
                        skill={this.state.selectedSkill}
                        open={this.state.openSkillCreateComponent}
                        onCreateSkillSubmit={this.onAddSkillSubmit}
                        onCancel={this.onAddButton}/>
                    <SkillListComponent
                        skills={this.props.skills}
                        onSkillDelete={this.onDeleteSkillRequest}
                        onSkillEdit={this.onSelectSkill}
                    />

                </div>
                <ConfirmDeleteDialogComponent
                    open={this.state.openJobDialogDeleteComponent}
                    title={"Are you sure you want to delete the following skill"}
                    info={this.state.selectedSkill ? this.state.selectedSkill.name : null}
                    onAgree={this.onSkillDialogDeleteComponentAgree}
                    onDisagree={this.onSkillDialogDeleteComponentDisagree}
                />
            </div>
        )
    }

    componentDidMount() {
        log(`componentDidMount`);
    }

    componentWillUnmount() {
        log(`componentWillUnmount`);
        this.props.resetAction();
    }
}

const mapStateToProps = (state) => ({
    skills: state.app.skills
});
const mapDispatchToProps = (dispatch) => ({
    addSkill: (skill) => dispatch(addSkill(skill)),
    deleteSkill: (skillId) => dispatch(deleteSkill(skillId)),
    resetAction: () => dispatch(resetAction()),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(SkillPage));

export default withConnect;