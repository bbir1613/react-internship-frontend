import React from 'react';
import TextField from 'material-ui/TextField';
import {withStyles} from 'material-ui/styles';
import Button from 'material-ui/Button';
import DeleteIcon from 'material-ui-icons/Delete';
import Card from "material-ui/Card";

const styles = {
    root: {
        display: 'flex',
        margin: 10,
        marginTop: 15,
        alignItems: 'center'
    },
    innerDiv: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        flex: 1,
    },
    input: {
        width: "10%",
        margin: 5,
    },
    button: {
        width: "10%"
    },
    select: {
        width: '30%',
        margin: 5
    }
};

const JobSkillComponent = (props) => {
    const {classes, skill, onChange, onRemove, availableSkills, useValue} = props;
    if (skill.remove) return null;

    const skillId = skill ? `${skill.skillId}` : '';

    return (<Card className={classes.root}>
        <div className={classes.innerDiv}>
            <TextField className={classes.select} select label='SkillName' name='skillId'
                       onChange={onChange('skillId')}
                       value={useValue ? skillId : undefined}
                       defaultValue={!useValue ? skillId : undefined}
                       SelectProps={{
                           native: true,
                           MenuProps: {
                               className: classes.menu,
                           },
                       }}>
                {availableSkills.map((skill, index) => {
                    return (
                        <option id={index} key={index} value={skill.id}>
                            {skill.name}
                        </option>
                    )
                })}
            </TextField>
            <TextField
                label='rating'
                value={skill.rating}
                onChange={onChange('rating')}
                type="number"
                className={classes.input}
                InputLabelProps={{
                    shrink: true,
                }}
                margin="normal"
            />
        </div>
        <div>
            <Button className={classes.button} size="small" onClick={onRemove}> <DeleteIcon/> </Button>
        </div>
    </Card>)
};

export default withStyles(styles)(JobSkillComponent);