import React, {Fragment} from 'react';
import AddIcon from 'material-ui-icons/Add';
import {withStyles} from 'material-ui/styles';
import Button from "material-ui/Button";
import JobSkillComponent from './skill';
import Divider from 'material-ui/Divider';

const styles = {
    root: {
        marginTop: 5,
        display: 'flex',
        alignItems: 'center'
    },
    innerDiv: {
        flex: 1,
    },
    input: {},
    button: {}
};

const JobSkillsComponent = (props) => {
    const {classes, title, add, skills, availableSkills, onRemove, onChange, useValue = false} = props;
    return (<Fragment>
        <div className={classes.root}>
            <div className={classes.innerDiv}>
                {title}
            </div>
            <div>
                <Button size="small" onClick={add}><AddIcon/></Button>
            </div>
        </div>
        <Divider className={classes.divider}/>
        {skills.map((skill, index) =>
            <JobSkillComponent key={index}
                               onRemove={onRemove(index)}
                               useValue={useValue}
                               onChange={onChange(index)}
                               availableSkills={availableSkills}
                               skill={skill}/>
        )}
    </Fragment>)
};

export default withStyles(styles)(JobSkillsComponent);