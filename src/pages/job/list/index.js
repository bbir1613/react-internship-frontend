import React, {Fragment} from 'react';
import {withRouter} from 'react-router-dom';
import Table, {TableBody, TableCell, TableHead, TableRow} from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import DeleteIcon from 'material-ui-icons/Delete';
import EditIcon from 'material-ui-icons/Edit';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import Divider from 'material-ui/Divider';
import {withStyles} from 'material-ui/styles';

const styles = {
    root: {
        marginTop: 10,
        marginBottom: 10,
    },
    table: {},
    title: {
        margin: 10
    }
};


const redirect = (history, id) => history.push(`/company/job/${id}`);

const JobListComponent = (props) => {
    const {classes, jobs, onDeleteJob, onEditJob, history} = props;
    return (
        <Paper className={classes.root}>
            {jobs.length ?
                <Fragment>
                    <Typography variant="display2" className={classes.title}>
                        Jobs
                    </Typography>
                    <Divider/>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell>Name</TableCell>
                                <TableCell>Description</TableCell>
                                <TableCell>Company</TableCell>
                                <TableCell/>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {jobs.map((job, index) => {
                                return (
                                    <TableRow key={index} hover={true}>
                                        <TableCell size="small"
                                                   onClick={() => redirect(history, job.id)}> {job.name} </TableCell>
                                        <TableCell size="small"
                                                   onClick={() => redirect(history, job.id)}> {job.description}</TableCell>
                                        <TableCell size="small"
                                                   onClick={() => redirect(history, job.id)}> {job.companyInfo.name}</TableCell>
                                        <TableCell>
                                            <Button size="small" onClick={() => onDeleteJob(job)}><DeleteIcon/></Button>
                                            <Button size="small" onClick={() => onEditJob(job)}><EditIcon/></Button>
                                        </TableCell>
                                    </TableRow>
                                )
                            })}
                        </TableBody>
                    </Table>
                </Fragment>
                :
                null
            }
        </Paper>
    )
};

export default withStyles(styles)(withRouter(JobListComponent));