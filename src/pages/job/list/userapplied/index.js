import React, {Fragment} from 'react';
import {withRouter} from 'react-router-dom';
import Table, {TableBody, TableCell, TableHead, TableRow} from 'material-ui/Table';
import Paper from "material-ui/Paper";
import Typography from 'material-ui/Typography';
import Divider from 'material-ui/Divider';
import {withStyles} from 'material-ui/styles';

const styles = {
    root: {
        marginTop: 10,
        marginBottom: 10,
        width: "35%"
    },
    table: {},
    title: {
        margin: 10
    }
};
const JobsUserAppliedForListComponent = (props) => {
    const {classes, appliedJobs, switchRoute} = props;

    return (
        <Paper className={classes.root}>
            {appliedJobs.length ?
                <Fragment>
                    <Typography variant="display2" className={classes.title}>
                        Jobs
                    </Typography>
                    <Divider/>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Job</TableCell>
                                <TableCell>Status</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {appliedJobs.map((job, index) => {
                                return (
                                    <TableRow key={index} hover={true}>
                                        <TableCell onClick={() => switchRoute(`/user/job/${job.JobId}`)}>
                                            {job.jobInfo.name}
                                        </TableCell>
                                        <TableCell>
                                            {job.isAccepted ? 'true' : 'false'}
                                        </TableCell>
                                    </TableRow>
                                )
                            })}
                        </TableBody>
                    </Table>
                </Fragment>
                : null}
        </Paper>
    )
};

export default withStyles(styles)(withRouter(JobsUserAppliedForListComponent));