import React from 'react';
import {withStyles} from 'material-ui/styles';
import Card, {CardContent, CardHeader} from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import Divider from 'material-ui/Divider';
import Avatar from 'material-ui/Avatar';
// import {Link} from "react-router-dom";
import moment from 'moment';

// import getLogger from '../../../../util/logger';


// const log = getLogger("JobListCardComponent");

const styles = {
    root: {
        padding: 25,
        width: '30%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    card: {
        width: '100%',
        textAlign: 'center',
        margin: 15,
    },
    company: {
        marginTop: 10,
        flex: 1
    },
    content: {
        display: 'flex',
        marginLeft: 5,
    },
    header: {
        backgroundImage: "url('http://www.desprejoburi.com/wp-content/uploads/2015/03/huntsy-a-smart-dashboard-for-your-job-search-691ee3b7b5.jpg')"
    }
};

const JobListCardComponent = (props) => {
    const {classes, jobs, switchRoute, onClick} = props;

    const createdDate = (date) => {
        date = moment(date);
        return moment().diff(date, 'days') > 1 ? date.format('YYYY-MM-DD') : date.fromNow();
    };
    return (
        <div className={classes.root}>
            {jobs.length ?
                jobs.map((job, index) => {
                    return (
                        <Card className={classes.card} key={index}>
                            <CardHeader
                                className={classes.header}
                                title={job.name}
                                subheader={createdDate(job.createdAt)}
                                // component={Link}
                                // to={`/user/job/${job.id}`}
                                onClick={() => switchRoute(`/user/job/${job.id}`)}
                            />
                            <Divider/>
                            <CardContent onClick={() => onClick(job.companyInfo.name)}>
                                <div className={classes.content}>
                                    {job.companyInfo.contactInfo.avatarUrl && <Avatar className={classes.bigAvatar}
                                                                                      src={job.companyInfo.contactInfo.avatarUrl}/>}
                                    <Typography className={classes.company}>
                                        {job.companyInfo.name}
                                    </Typography>
                                </div>
                            </CardContent>
                        </Card>
                    );
                })
                :
                null}
        </div>)
};

export default withStyles(styles)(JobListCardComponent);