import React from 'react';
import {withStyles} from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import DeleteIcon from 'material-ui-icons/Delete';
import Card from "material-ui/Card";

const styles = {
    root: {
        display: 'flex',
        margin: 10,
        marginTop: 15,
        alignItems: 'center'
    },
    innerDiv: {
        flex: 1,
    },
    input: {
        width: "85%",
        margin: 2
    },
    button: {}
};

const JobRequirementComponent = (props) => {
    const {classes, requirement, onChange, onRemove} = props;
    if (requirement.remove) return null;
    return (
        <Card className={classes.root}>
            <div className={classes.innerDiv}>
                <TextField
                    className={classes.input}
                    label="name"
                    onChange={onChange("name")}
                    value={requirement.name}
                />
            </div>
            <div>
                <Button className={classes.button} size="small" onClick={onRemove}> <DeleteIcon/> </Button>
            </div>
        </Card>
    )
};

export default withStyles(styles)(JobRequirementComponent);