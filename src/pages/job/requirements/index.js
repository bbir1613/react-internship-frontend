import React, {Fragment} from 'react';
import {withStyles} from 'material-ui/styles';
import Button from "material-ui/Button";
import AddIcon from 'material-ui-icons/Add';
import JobRequirementComponent from './requirement';
import Divider from 'material-ui/Divider';

const styles = {
    root: {
        marginTop: 5,
        display: 'flex',
        alignItems: 'center'
    },
    innerDiv: {
        flex: 1,
    },
    input: {},
    button: {}
};

const JobRequirementsComponent = (props) => {
    const {classes, title, add, requirements, onRemove, onChange} = props;
    return (<Fragment>
            <div className={classes.root}>
                <div className={classes.innerDiv}>
                    {title}
                </div>
                <div>
                    <Button size="small" onClick={add}><AddIcon/></Button>
                </div>
            </div>
            <Divider className={classes.divider}/>
            {requirements.map((requirement, index) =>
                <JobRequirementComponent
                    onRemove={onRemove(index)}
                    key={index}
                    requirement={requirement}
                    onChange={onChange(index)}/>
            )}
        </Fragment>
    )
};

export default withStyles(styles)(JobRequirementsComponent);