import * as JobTypeActions from '../constants';
import {RESET_ACTION} from "../../app/constants";

let reduce = {};

reduce[JobTypeActions.GET_ALL_JOBS_SUCCEEDED] = (state, action) => ({...state, jobs: action.payload});
reduce[JobTypeActions.GET_JOBS_USER_APPLIED_FOR_SUCCEEDED] = (state, action) => ({...state, appliedJobs: action.payload});
reduce[JobTypeActions.GET_JOB_INFO_SUCCEEDED] = (state, action) => ({...state, jobInfo: action.payload});
reduce[RESET_ACTION] = (state, action) => (initialState());

const initialState = ()=>({
    jobs: [],
    appliedJobs: [],
    jobInfo: undefined,
});

const reducer = (state = initialState(), action) => reduce[action.type] ? reduce[action.type](state, action) : state;

export default reducer;