import React, {Fragment} from 'react';
import {withStyles} from 'material-ui/styles';
import {Button} from "material-ui";
import Card, {CardActions, CardContent} from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import Chip from 'material-ui/Chip';
import Divider from 'material-ui/Divider';
// import getLogger from '../../../../util/logger';


// const log = getLogger("JobListCardComponent");

const styles = {
    root: {
        marginTop: 30,
        display: 'flex',
        justifyContent: 'center'
    },
    chip: {
        margin: 5
    },
    subtitle: {
        marginTop: 15
    },
    title:{
        backgroundImage: "url('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSlqKbqoZYBz3JimirAFqRZ0y18Shni0kqihOEs0RC9mMWRcLgO')",
        height: "111.5px",
        display:'flex',
        alignItems:'center',
        justifyContent:'center'
    },
    card: {
        width: "50%"
    }

};

const appliedForJob = (jobs, userId) => jobs.reduce((initial, job) => initial || job.userId === userId, false);

const JobDetailComponent = (props) => {
    const {classes, job, user, onApplyJob} = props;
    return (
        <div className={classes.root}>
            {job ?
                <Card className={classes.card}>
                    <CardContent>
                        <div className={classes.title}>
                            <Typography variant="headline" component="h2" align="center">
                                {job.name}
                            </Typography>
                        </div>
                        <div className={classes.subtitle}>
                            <Typography variant="headline" align='center'>
                                Description:<br/>
                            </Typography>
                            <br/><br/>
                            <Typography variant="body1" align="center">
                                {job.description ? job.description : ''}<br/><br/>
                            </Typography>
                        </div>
                        <Typography variant="body2">
                            Company: {job.companyInfo ? job.companyInfo.name : ''}
                        </Typography>
                        <br/><br/>
                        <Typography variant="body2">
                            JobRequirements:
                        </Typography>
                        <Divider/>
                        {job.jobRequirementInfoList ?
                            <ul>
                                {job.jobRequirementInfoList.map((require, index) => {
                                    return <li key={index}> {require.name}</li>
                                })}
                            </ul>
                            :
                            null
                        }
                        <Typography variant="body2">
                            JobBenefits:
                        </Typography>
                        <Divider/>
                        {job.jobBenefitInfoList ?
                            <ul>
                                {job.jobBenefitInfoList.map((benefit, index) => {
                                    return <li key={index}> {benefit.name}</li>
                                })}
                            </ul>
                            :
                            null}
                        <Typography variant="body2">
                            JobSkills:
                        </Typography>
                        <Divider/>
                        {job.jobSkillInfoList ?
                            <ul>
                                {job.jobSkillInfoList.map((skill, index) => {
                                    return (
                                        <Fragment key={index}>
                                            <Chip
                                                label={`${skill.skillInfo.name} rating: ${skill.rating}`}
                                                className={classes.chip}
                                            />
                                        </Fragment>
                                    )
                                })}
                            </ul>
                            :
                            null
                        }
                    </CardContent>
                    <CardActions>
                        <Button disabled={appliedForJob(job.userJobApplicationInfoList, user.id)}
                                onClick={() => onApplyJob(job)}>apply</Button>
                    </CardActions>
                </Card>
                :
                null}
        </div>)

};

export default withStyles(styles)(JobDetailComponent);