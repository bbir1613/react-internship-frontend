import http from "../../../configs/http";
import {
    COMPANIES,
    JOBS,
    USERJOBAPPLICATIONS,
    JOBSKILLS,
    JOBBENEFITS,
    JOBREQUIREMENT
} from "../../../constants/api-routes";
import * as JobTypeAction from '../constants';
import {setNotification, setError} from '../../../components/notification/actions';
import getLogger from '../../../util/logger';
import {getCompaniesSucceeded} from "../../company/actions";

const log = getLogger('job-action', false);

export const filterJobsByCompanyName = (companiesName) => async dispatch => {
    log(`filterJobsByCompanyName`);
    try {
        // const jobs = (await http.get(`${JOBS}`)).data
        //     .filter(job => job.isAvailable && companiesName.findIndex(name => job.companyInfo.name === name) > -1);
        //
        // const mappedList = [];
        //
        // for (let job of jobs ) {
        //     const contactInfo = (await http.get(`/contacts/${job.companyInfo.contactInfoId}`)).data;
        //
        //     mappedList.push({
        //         ...job,
        //         companyInfo: {
        //             ...job.companyInfo,
        //             contactInfo
        //         }
        //     });
        // }

        const jobs = await Promise.all((await http.get(`${JOBS}`)).data
            .filter(job => job.isAvailable && companiesName.findIndex(name => job.companyInfo.name === name) > -1)
            .map(async (job) => {
                const contactInfo = (await http.get(`/contacts/${job.companyInfo.contactInfoId}`)).data;
                return {
                    ...job,
                    companyInfo: {
                        ...job.companyInfo,
                        contactInfo
                    }
                }
            }));
        log(`jobs:`, jobs);
        dispatch(getAllJobsSucceeded(jobs));
    } catch (error) {
        log(`filterJobsByCompanyName`);
        dispatch(setError(error.response.statusText));
    }
};

export const getAllJobs = () => async dispatch => {
    log(`getAllJobs`);
    try {
        const jobs = await Promise.all((await http.get(`${JOBS}`)).data.filter(job => job.isAvailable).map(async (job) => {
            const contactInfo = (await http.get(`/contacts/${job.companyInfo.contactInfoId}`)).data;
            return {
                ...job,
                companyInfo: {
                    ...job.companyInfo,
                    contactInfo
                }
            }
        }));
        log(jobs);
        dispatch(getAllJobsSucceeded(jobs));
    } catch (error) {
        log(`getAllJobs error`, error);
        dispatch(setError(error.statusText));
    }
};

export const getJobInfoById = (jobId) => dispatch => {
    log(`getJobInfoById`, jobId);
    return http.get(`${JOBS}/${jobId}`)
        .then(response => {
            log(`getJobInfoById success`, response.data);
            dispatch(getJobInfoSucceeded(response.data));
            return response.data;
        }).catch(error => {
            log(`getJobInfoById error`);
            dispatch(setError(error.response.statusText));
        })
};

export const getAllJobsByUserId = (userId) => dispatch => {
    log(`getAllJobsByUserId`);
    return http.get(`${JOBS}`)
        .then(response => {
            log(`getAllJobsByUserId success`, response.data);
            const jobs = response.data.filter(job => job.companyInfo.userId === userId && job.isAvailable === true);
            log(`my jobs`, jobs);
            dispatch(getAllJobsSucceeded(jobs));
        }).catch(error => {
            log(`getAllJobsByUserId error`, error);
            dispatch(setError(error.response.statusText));
        });
};

export const getJobsByCompanyId = (companyId) => dispatch => {
    log(`getJobsByCompanyId`, companyId);
    return http.get(`${COMPANIES}/${companyId}/true`)
        .then(response => {
            log(`getJobsByCompanyId success`, response.data);
            const {jobs, company} = mapCompanyToJobs({...response.data});
            log(`available jobs : `, jobs);
            dispatch(getAllJobsSucceeded(jobs));
            dispatch(getCompaniesSucceeded([company]));
        }).catch(error => {
            log(`getJobsByCompanyIderror`, error);
            dispatch(setError(error.response.statusText));
        });
};

export const getJobsUserAppliedFor = (userId) => dispatch => {
    log(`getJobsUserAppliedFor`, userId);
    return http.get(`${USERJOBAPPLICATIONS}/user/${userId}`)
        .then(response => {
            log(`getJobsUserAppliedFor succes`, response.data);
            dispatch(getJobsUserAppliedForSucceeded(response.data));
        }).catch(error => {
            log(`getJobsUserAppliedFor error`, error);
            dispatch(setError(error.response.statusText));
        });
};

export const addJob = (job, jobDetails) => async dispatch => {
    try {
        const response = await http.post(`${JOBS}`, job);
        const jobId = response.data.id;

        for (let i = 0; i < jobDetails.requirements.length; ++i) await  addRequirement(jobDetails.requirements[i], jobId);
        for (let i = 0; i < jobDetails.benefits.length; ++i) await  addBenefit(jobDetails.benefits[i], jobId);
        for (let i = 0; i < jobDetails.skills.length; ++i) await addSkill(jobDetails.skills[i], jobId);

        dispatch(setNotification(`Job added with success`));
    } catch (error) {
        log(`addJob error`, error);
        dispatch(setError(error.response.statusText));
    }
};

export const updateJob = (job, jobDetails) => async dispatch => {
    try {
        if (jobDetails) {
            const jobId = job.id;
            for (let i = 0; i < jobDetails.requirements.length; ++i) await  addRequirement(jobDetails.requirements[i], jobId);
            for (let i = 0; i < jobDetails.benefits.length; ++i) await  addBenefit(jobDetails.benefits[i], jobId);
            for (let i = 0; i < jobDetails.skills.length; ++i) await addSkill(jobDetails.skills[i], jobId);
        }
        await http.put(`${JOBS}/${job.id}`, job);
        job.isAvailable ? dispatch(setNotification(`Job updated with success`)) : dispatch(setNotification(`Job deleted with success`));
    } catch (error) {
        log(`updateJob error`, error);
        dispatch(setError(error.response.statusText));
    }
};

export const removeSkill = (id) => dispatch => {
    log(` removeSkill `, id);
    return http.delete(`${JOBSKILLS}/${id}`);
};

export const removeBenefit = (id) => dispatch => {
    log(` removeBenefit `, id);
    return http.delete(`${JOBBENEFITS}/${id}`);
};

export const removeRequirement = (id) => dispatch => {
    log(` removeRequirement `, id);
    return http.delete(`${JOBREQUIREMENT}/${id}`);
};

const addSkill = (skill, jobId) => {
    if (skill.id) {
        return http.put(`${JOBSKILLS}/${skill.id}`, skill);
    } else {
        return http.post(`${JOBSKILLS}`, {...skill, jobId});
    }
};

const addBenefit = (benefit, jobId) => {
    if (benefit.id) {
        return http.put(`${JOBBENEFITS}/${benefit.id}`, benefit);
    } else {
        return http.post(`${JOBBENEFITS}`, {...benefit, jobId});
    }
};

const addRequirement = (requirement, jobId) => {
    if (requirement.id) {
        return http.put(`${JOBREQUIREMENT}/${requirement.id}`, requirement);
    } else {
        return http.post(`${JOBREQUIREMENT}`, {...requirement, jobId});
    }
};

export const userApplyToJob = (jobId, userId) => dispatch => {
    log(`userApplyToJob`, jobId);
    return http.post(`${USERJOBAPPLICATIONS}`, {
        jobId,
        userId
    })
        .then(response => {
            log(`userApplyToJob success`, response.data);
            dispatch(setNotification(`Applied for job with success`));
        }).catch(error => {
            log(`userApplyToJob error`, error);
            dispatch(setError(error.response.statusText));
        });
};

export const acceptUserToJob = (user) => dispatch => {
    log(`acceptUserToJob`, user);
    return http.put(`${USERJOBAPPLICATIONS}/${user.id}`, user)
        .then(response => {
            log(`acceptUserToJob success`, response.data);
            dispatch(setNotification(" User accepted "));
        }).catch(error => {
            log(`acceptUser error`, error);
            dispatch(setError(error.response.statusText));
        })
};

const mapCompanyToJobs = (company) => {
    const allJobs = [...company.jobInfoList];
    delete company.jobInfoList;
    const jobs = allJobs.map((job) => ({
        ...job,
        companyInfo: {...company}
    })).filter(job => job.isAvailable === true);
    return {jobs, company: {...company}};
};


const getAllJobsSucceeded = (payload) => ({type: JobTypeAction.GET_ALL_JOBS_SUCCEEDED, payload});
const getJobsUserAppliedForSucceeded = (payload) => ({
    type: JobTypeAction.GET_JOBS_USER_APPLIED_FOR_SUCCEEDED,
    payload
});
const getJobInfoSucceeded = (payload) => ({
    type: JobTypeAction.GET_JOB_INFO_SUCCEEDED,
    payload
});