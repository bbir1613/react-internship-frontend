import React, {Fragment} from 'react';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import Dialog, {DialogActions, DialogContent, DialogTitle,} from 'material-ui/Dialog';
import JobRequirementsComponent from '../requirements';
import JobSkillsComponent from '../skills';
import {withStyles} from 'material-ui/styles';
import getLogger from '../../../util/logger';

const log = getLogger("JobCreateComponent");

const styles = {
    root: {
        width: '20%',
        marginLeft: '5%'
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
    },
    input: {},
};

const JobCreateComponent = (props) => {
    const {
        classes,
        onCreateJobSubmit,
        companies,
        job,
        open,
        onClose,
        jobDetails,
        onRequirementChange,
        onBenefitChange,
        onSkillChange,
        addRequirement,
        addBenefits,
        addSkills,
        onRequirementRemove,
        onBenefitRemove,
        onSkillRemove,
        availableSkills
    } = props;

    const onSubmit = (event) => {
        event.preventDefault();
        const name = event.target.name.value;
        const description = event.target.description.value;
        const companyId = event.target.companyId.value;
        const isAvailable = true;
        const id = job ? job.id : undefined;
        log(`onSubmit`, name, description, companyId, isAvailable, id);
        onCreateJobSubmit({
            id,
            name,
            description,
            isAvailable,
            companyId
        });
    };

    return (
        <Dialog
            open={open}
            onClose={onClose}
            fullWidth={true}
        >
            <DialogTitle>{job ? 'Edit job' : 'Create job'}</DialogTitle>
            <form onSubmit={onSubmit}>
                <DialogContent className={classes.form}>
                    <TextField className={classes.input} label='name' name='name' defaultValue={job ? job.name : ''}/>
                    <TextField className={classes.input} label='description' name='description'
                               defaultValue={job ? job.description : ''}/>
                    <TextField className={classes.input} select label='company' name='companyId'
                               defaultValue={job ? `${job.companyId}` : ''}
                               SelectProps={{
                                   native: true,
                                   MenuProps: {
                                       className: classes.menu,
                                   },
                               }}>
                        {companies.map(company => {
                            return (
                                <option key={company.id} value={company.id}>
                                    {company.name}
                                </option>
                            )
                        })}
                    </TextField>
                    <Fragment>
                        <JobRequirementsComponent
                            title={"requirements"}
                            requirements={jobDetails.requirements}
                            add={addRequirement}
                            onChange={onRequirementChange}
                            onRemove={onRequirementRemove}
                        />
                        <JobRequirementsComponent
                            title={"benefits"}
                            requirements={jobDetails.benefits}
                            add={addBenefits}
                            onChange={onBenefitChange}
                            onRemove={onBenefitRemove}
                        />
                        <JobSkillsComponent
                            title={"skills"}
                            skills={jobDetails.skills}
                            add={addSkills}
                            onRemove={onSkillRemove}
                            onChange={onSkillChange}
                            availableSkills={availableSkills}
                            useValue={true}
                        />
                    </Fragment>
                </DialogContent>
                <DialogActions>
                    <Button onClick={onClose} color="primary">
                        Cancel
                    </Button>
                    <Button type="submit" color="primary">
                        {job ? 'Edit' : 'Create'}
                    </Button>
                </DialogActions>
            </form>
        </Dialog>
    )
};

export default withStyles(styles)(JobCreateComponent);