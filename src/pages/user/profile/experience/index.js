import React, {Fragment} from 'react';
import {withStyles} from 'material-ui/styles';
import Button from "material-ui/Button";
import AddIcon from 'material-ui-icons/Add';
import Divider from "material-ui/Divider";
import UserInstitutionsComponent from "./institutions";

const styles = {
    root: {},
    divider: {
        width: "50%",
        alignSelf: 'center',
    },
    flex: {
        display: 'flex',
        width: '50%',
        alignSelf: 'center',
        alignItems:'center',
    },
    grow: {
        flex: 1,
    },
};

const UserExperienceComponent = (props) => {
    const {classes, details, addProp, onChange, onRemove, title} = props;
    return (
        <Fragment>
            <div className={classes.flex}>
                <div className={classes.grow}>
                    {title}
                </div>
                <div>
                    <Button size="small" onClick={addProp}><AddIcon/></Button>
                </div>
            </div>
            <Divider className={classes.divider}/>
            <UserInstitutionsComponent
                details={details}
                onChange={onChange}
                onRemove={onRemove}
            />
        </Fragment>);
};

export default withStyles(styles)(UserExperienceComponent);