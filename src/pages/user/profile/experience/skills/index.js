import React, {Fragment} from 'react';
import AddIcon from 'material-ui-icons/Add';
import {withStyles} from 'material-ui/styles';
import Button from "material-ui/Button";
import UserSkillComponent from './skill';
import Divider from 'material-ui/Divider';

const styles = {
    root: {},
    divider: {
        width: "50%",
        alignSelf: 'center',
    },
    flex: {
        display: 'flex',
        width: '50%',
        alignSelf: 'center',
        alignItems: 'center',
    },
    grow: {
        flex: 1,
    },
};

const UserSkillsComponent = (props) => {
    const {classes, title, add, skills, availableSkills, onRemove, onChange, useValue = false} = props;
    return (<Fragment>
        <div className={classes.flex}>
            <div className={classes.grow}>
                {title}
            </div>
            <div>
                <Button size="small" onClick={add}><AddIcon/></Button>
            </div>
        </div>
        <Divider className={classes.divider}/>
        {skills.map((skill, index) =>
            <UserSkillComponent key={index}
                                onRemove={onRemove(index)}
                                useValue={useValue}
                                onChange={onChange(index)}
                                availableSkills={availableSkills}
                                skill={skill}/>
        )}
    </Fragment>)
};

export default withStyles(styles)(UserSkillsComponent);