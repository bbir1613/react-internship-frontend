import React from 'react';
import TextField from 'material-ui/TextField';
import {withStyles} from 'material-ui/styles';
import Button from 'material-ui/Button';
import DeleteIcon from 'material-ui-icons/Delete';
import Card from "material-ui/Card";

const styles = {
    root: {
        minWidth: '50%',
        display: 'flex',
        alignSelf: 'center',
        alignItems: 'center',
        margin: 10,
    },
    innerDiv: {
        flex: 1,
        justifyContent: "space-between"
    },
    input: {
        minWidth: "10%",
        margin: 10,
    },
    button: {
        minWidth: "10%"
    },
    select: {
        minWidth: '30%',
        margin: 10
    }
};

const UserSkillComponent = (props) => {
    const {classes, skill, onChange, onRemove, availableSkills, useValue} = props;
    if (skill.remove) return null;
    return (<Card className={classes.root}>
        <div className={classes.innerDiv}>
            <TextField className={classes.select} select label='SkillName' name='skillId'
                       onChange={onChange('skillId')}
                       value={useValue ? skill ? `${skill.skillInfo.id}` : '' : undefined}
                       defaultValue={useValue ? undefined : skill && skill.skillInfo ? `${skill.skillInfo.id}` : ''}
                       SelectProps={{
                           native: true,
                           MenuProps: {
                               className: classes.menu,
                           },
                       }}>
                {availableSkills.map((skill, index) => {
                    return (
                        <option id={index} key={index} value={skill.id}>
                            {skill.name}
                        </option>
                    )
                })}
            </TextField>
            <TextField
                label='rating'
                value={skill.rating}
                onChange={onChange('rating')}
                type="number"
                className={classes.input}
                InputLabelProps={{
                    shrink: true,
                }}
                margin="normal"
            />
        </div>
        <div>
            <Button className={classes.button} size="small" onClick={onRemove}> <DeleteIcon/> </Button>
        </div>
    </Card>)
};

export default withStyles(styles)(UserSkillComponent);