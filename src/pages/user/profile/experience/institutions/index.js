import React, {Fragment} from 'react';
import UserInstitutionComponent from './institution';
import {withStyles} from 'material-ui/styles';

const styles = {
    root: {},
};

const UserInstitutionsComponent = (props) => {
    const {details, onChange, onRemove} = props;
    return (
        <Fragment>
            {details.map((detail, index) => <UserInstitutionComponent
                key={index}
                details={detail}
                onChange={onChange(index)}
                onRemove={onRemove(index)}
            />)}
        </Fragment>
    )
};

export default withStyles(styles)(UserInstitutionsComponent);