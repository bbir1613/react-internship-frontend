import React from 'react';
import TextField from 'material-ui/TextField';
import {withStyles} from 'material-ui/styles';
import DeleteIcon from 'material-ui-icons/Delete';
import Button from 'material-ui/Button';
import Card from 'material-ui/Card';
import moment from 'moment';

const styles = {
    root: {
        width: '50%',
        display: 'flex',
        alignSelf: 'center',
        alignItems: 'center',
        margin: 10,
    },
    innerDiv: {
        flex: 1,
        justifyContent: "space-between"
    },
    input: {
        margin: 10,
    },
    button: {
        width: "10%"
    },
    datePicker: {
        margin: 10
    }
};

const UserInstitutionComponent = (props) => {
    const {classes, details, onRemove, onChange} = props;
    if (details.remove) return null;
    return (<Card className={classes.root}>
        <div className={classes.innerDiv}>
            <TextField className={classes.input}
                       label='institution'
                       value={details.institution}
                       onChange={onChange('institution')}
            />
            <TextField className={classes.input}
                       label='description'
                       value={details.description}
                       onChange={onChange('description')}
            />
            <TextField
                className={classes.datePicker}
                label="startDate"
                type="date"
                defaultValue={moment(details.startDate).format('YYYY-MM-DD')}
                InputLabelProps={{
                    shrink: true,
                }}
                onChange={onChange("startDate")}
            />
            <TextField
                className={classes.datePicker}
                label="endDate"
                type="date"
                defaultValue={moment(details.endDate).format('YYYY-MM-DD')}
                InputLabelProps={{
                    shrink: true,
                }}
                onChange={onChange("endDate")}
            />
        </div>
        <div>
            <Button className={classes.button} size="small" onClick={onRemove}> <DeleteIcon/> </Button>
        </div>
    </Card>)

};

export default withStyles(styles)(UserInstitutionComponent);