import React from 'react';
import Typography from 'material-ui/Typography';
import Avatar from 'material-ui/Avatar';
import Card from 'material-ui/Card';
import Divider from 'material-ui/Divider';
import EmailIcon from 'material-ui-icons/Email';
import PhoneIcon from 'material-ui-icons/Phone';
import LocationCityIcon from 'material-ui-icons/LocationCity';
import LocationOnIcon from 'material-ui-icons/LocationOn';
import WebIcon from 'material-ui-icons/Web';
import {withStyles} from 'material-ui/styles';

const styles = {
    root: {
        margin: 10
    },
    flex: {
        display: 'flex',
    },
    about: {
        marginBottom: 20,
        marginTop: 20,
    },
    width: {
        width: "100%"
    },
    generalInfoDiv: {
        marginTop: 30,
        display: 'flex',
        justifyContent: 'space-evenly',
        width: "100%"
    },
    rootDiv: {
        display: 'flex',
        margin: 15,
    },
    bigAvatar: {
        margin: 30,
        width: 70,
        height: 70,
    },
    column: {
        margin: 10,
        display: 'flex',
        flexDirection: 'column',
    },
    icon: {
        display: 'flex',
        alignItems: 'center'
    }
};

const getCountryNameById = (countries, countryId) => {
    for (let i = 0; i < countries.length; ++i) {
        if (countries[i].id === countryId) return countries[i].name
    }
    return ''
};

const PreviewContactInfoComponent = (props) => {
    const {classes, contactInfo, countries} = props;
    return (
        <div className={classes.root}>
            <div>
                <Typography variant="body2"> General information: </Typography>
                <Divider/>
            </div>
            <Card className={classes.rootDiv}>
                <div>
                    <Avatar className={classes.bigAvatar} src={contactInfo.avatarUrl ? contactInfo.avatarUrl : ''}/>
                </div>
                <div className={classes.width}>
                    <div className={classes.generalInfoDiv}>
                        <div>
                            <div className={classes.icon}>
                                <EmailIcon/>
                                &nbsp;
                                <Typography variant="body1">{contactInfo.email ? contactInfo.email : '-'}</Typography>
                            </div>
                            <div className={classes.icon}>
                                <PhoneIcon/>
                                &nbsp;
                                <Typography variant="body1">{contactInfo.phone ? contactInfo.phone : '-'}</Typography>
                            </div>
                            <div className={classes.icon}>
                                <WebIcon/>
                                &nbsp;
                                <Typography
                                    variant="body1">{contactInfo.website ? contactInfo.website : '-'}</Typography>
                            </div>
                        </div>
                        <div className={classes.margin}>
                            <div className={classes.icon}>
                                <Typography variant="body2"> Country: </Typography>
                                &nbsp;
                                <Typography
                                    variant="body1">{getCountryNameById(countries, contactInfo.countryId)}</Typography>
                            </div>
                            <div className={classes.icon}>
                                <LocationCityIcon/>
                                &nbsp;
                                <Typography variant="body1">{contactInfo.city ? contactInfo.city : '-'}</Typography>
                            </div>
                            <div className={classes.icon}>
                                <LocationOnIcon/>
                                &nbsp;
                                <Typography
                                    variant="body1">{contactInfo.address ? contactInfo.address : '-'}</Typography>
                            </div>
                        </div>
                    </div>
                    <div className={classes.about}>
                        <Typography variant="body1">{contactInfo.about ? contactInfo.about : '-'}</Typography>
                    </div>
                </div>
            </Card>
        </div>
    )
};

export default withStyles(styles)(PreviewContactInfoComponent);