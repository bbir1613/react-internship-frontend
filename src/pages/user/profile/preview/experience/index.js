import React from 'react';
import Typography from 'material-ui/Typography';
import {withStyles} from 'material-ui/styles';
import moment from 'moment';
import Divider from "material-ui/Divider";
import Card from "material-ui/Card";


const styles = {
    root: {
        margin: 10,
    },
    card: {
        marginTop: 15,
    },
    rootDiv: {
        display: 'flex',
        margin: 10,
    },
    leftDiv: {},
    rightDiv: {
        marginLeft: 20,
    },
    descriptionDiv: {
        marginTop: 10
    }
};

const PreviewExperienceComponent = (props) => {
    const {classes, title, details} = props;
    return (
        <div className={classes.root}>
            <div>
                <Typography variant="body2"> {title} </Typography>
                <Divider/>
            </div>
            <Card className={classes.card}>
                {details.map((detail, index) => (
                    detail.remove ?
                        null
                        :
                        <div className={classes.rootDiv} key={index}>
                            <div className={classes.leftDiv}>
                                <Typography
                                    variant={"body2"}>{moment(detail.startDate).format('YYYY-MM-DD')} - {moment(detail.endDate).format('YYYY-MM-DD')} </Typography>
                            </div>
                            <div className={classes.rightDiv}>
                                <Typography variant={"title"}> {detail.institution}</Typography>
                                <div className={classes.descriptionDiv}>
                                    <Typography variant={"body1"}>{detail.description}</Typography>
                                </div>
                            </div>
                        </div>
                ))}
            </Card>
        </div>
    )
};

export default withStyles(styles)(PreviewExperienceComponent);