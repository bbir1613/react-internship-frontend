import React from 'react';
import Typography from 'material-ui/Typography';
import Chip from 'material-ui/Chip';
import Divider from 'material-ui/Divider';
import {withStyles} from 'material-ui/styles';

const styles = {
    root: {
        margin: 10,
    },
    chip: {
        margin: 10
    }
};

const PreviewSkillComponent = (props) => {
    const {classes, title, skills} = props;
    return (
        <div className={classes.root}>
            <div>
                <Typography variant="body2"> {title} </Typography>
                <Divider/>
            </div>
            {skills.map((skill, index) => (
                skill.remove ?
                    null
                    :
                    <Chip
                        key={index}
                        label={`${skill.skillInfo.name} rating: ${skill.rating}`}
                        className={classes.chip}
                    />
            ))}
        </div>
    )
};

export default withStyles(styles)(PreviewSkillComponent);