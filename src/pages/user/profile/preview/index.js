import React, {Fragment} from 'react';
import {withStyles} from 'material-ui/styles';
import PreviewContactInfoComponent from "./contactInfo";
import PreviewExperienceComponent from "./experience";
import SkillPreviewComponent from "./skill";
import Card from "material-ui/Card";
import Divider from "material-ui/Divider";
import Button from "material-ui/Button";

const styles = {
    root: {
        margin: 1,
        display: 'flex',
        justifyContent: 'center'
    },
    card: {
        minWidth: "50%",
        display: 'flex',
        flexDirection: 'column'
    },
    gridButton: {
        width: "10%",
        alignSelf: 'flex-end',
        margin: 2,
        marginRight: 10,
    }
};

const PreviewComponent = (props) => {
    const {classes, contactInfo, educations, works, skills, countries, saveButton} = props;

    return (
        <div className={classes.root}>
            <Card className={classes.card}>
                <PreviewContactInfoComponent
                    contactInfo={contactInfo}
                    countries={countries}
                />
                <PreviewExperienceComponent
                    details={educations}
                    title={"Education:"}
                />
                <PreviewExperienceComponent
                    details={works}
                    title={"Work:"}
                />
                <SkillPreviewComponent
                    skills={skills}
                    title={"Skills:"}
                />
                {saveButton ?
                    <Fragment>
                        <Divider/>
                        <Button className={classes.gridButton} type="submit"> Save </Button>
                    </Fragment>
                    :
                    null}
            </Card>
        </div>)

};

export default withStyles(styles)(PreviewComponent);