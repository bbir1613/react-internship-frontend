import React, {Component, Fragment} from 'react';
import {withStyles} from 'material-ui/styles';
import {connect} from 'react-redux';
import getLogger from '../../../util/logger';
import ProfileStepperComponent from './stepper';
import {isCompany} from "../../../constants/routes";
import * as UserActions from "../actions";
import ConfirmDeleteDialogComponent from '../../../components/ConfirmDialogComponent';
import _ from 'lodash';
import {getUserSkills, resetAction} from "../../app/actions";
import {removeFromObject} from "../../../util";

const log = getLogger("UserProfilePage");

const styles = {
    root: {
        margin: 15,
        width: "90%",
    }
};

const getInitialState = () => ({
    activeStep: 0,
    contactInfo: {
        email: '',
        phone: '',
        city: '',
        countryId: '0',
        address: '',
        website: '',
        avatarUrl: '',
        about: '',
    },
    userDetails: {
        works: [],
        educations: [],
    },
    skills: [],
    pathToSelectedItemToDelete: undefined,
    openJobDialogDeleteComponent: false
});

class UserProfilePage extends Component {

    state = getInitialState();

    getInitialData = async id => {
        const response = await this.props.getUserInfo(id);
        const skills = await this.props.getUserSkills(id);
        const contactInfo = {...response.contactInfo};
        const userDetails = {
            works: [...response.userWorkExperienceInfoList],
            educations: [...response.userEducationInfoList],
        };
        this.setState({
            ...this.state,
            contactInfo,
            userDetails,
            skills
        });
    };

    addProp = (...propName) => (prop) => () => {
        const newState = {...this.state};
        const path = propName.concat(prop);
        let obj = undefined;
        if (prop === "works" || prop === "educations") {
            obj = {
                id: undefined,
                userId: this.props.user.id,
                institution: '',
                description: '',
                startDate: new Date(),
                endDate: new Date(),
            };
        } else {
            obj = {
                id: undefined,
                userId: this.props.user.id,
                rating: 0,
                skillId: this.props.availableSkills[0].id,
                skillInfo: {...this.props.availableSkills[0]}
            };
        }
        const value = _.get(newState, path).concat(obj);
        _.set(newState, path, value);
        this.setState(newState);
    };

    onValueChange = (...propName) => index => prop => (event) => {
        const newState = {...this.state};
        let value = undefined;
        let path = undefined;
        if (prop === "skillId") {
            path = propName.concat(index);
            value = {
                ..._.get(newState, path),
                skillId: event.target.value,
                skillInfo: {...this.props.availableSkills[event.target.selectedOptions[0].id]}
            };
        } else {
            path = _.without(propName.concat(index, prop), null, undefined);
            value = event.target.value;
        }
        _.set(newState, path, value);
        this.setState(newState);
    };

    onRemove = (...propName) => (index) => () => {
        log(` onRemove`, propName, index);
        const pathToSelectedItemToDelete = propName.concat(index);
        this.setState({...this.state, pathToSelectedItemToDelete, openJobDialogDeleteComponent: true})
    };

    onCreateProfileSubmit = async (event) => {
        event.preventDefault();
        await this.props.addUserProfile(this.props.user, this.state.contactInfo, this.state.userDetails, this.state.skills);
        await this.getInitialData(this.props.user.id);
    };

    changeStep = (step, stepsLength) => () => this.setState(prevState => ({
        ...this.state,
        activeStep: prevState.activeStep === step ? stepsLength : step
    }));

    onConfirmDeleteDialogComponentAgree = async () => {
        log(` onConfirmDeleteDialogComponentAgree `, this.state.pathToSelectedItemToDelete);
        const request = {
            'works': this.props.removeUserWork,
            'educations': this.props.removeUserEducation,
            'skills': this.props.removeUserSkill
        };
        const newState = await removeFromObject({...this.state}, [...this.state.pathToSelectedItemToDelete], request);
        this.setState(newState,this.onConfirmDeleteDialogComponentDisagree);
    };

    onConfirmDeleteDialogComponentDisagree = () => {
        log(` onConfirmDeleteDialogComponentDisagree `);
        this.setState({...this.state, pathToSelectedItemToDelete: '', openJobDialogDeleteComponent: false})
    };

    render() {
        return (<Fragment>
            <ProfileStepperComponent
                companyUser={isCompany(this.props.user)}
                userInfo={this.props.userInfo}
                activeStep={this.state.activeStep}
                onSubmit={this.onCreateProfileSubmit}
                changeStep={this.changeStep}
                countries={this.props.countries}
                contactInfo={this.state.contactInfo}
                works={this.state.userDetails.works}
                educations={this.state.userDetails.educations}
                skills={this.state.skills}
                availableSkills={this.props.availableSkills}
                addProp={this.addProp}
                onValueChange={this.onValueChange}
                onRemove={this.onRemove}
            />
            <ConfirmDeleteDialogComponent open={this.state.openJobDialogDeleteComponent}
                                          title={"Are you sure you want to delete ?"}
                                          onAgree={this.onConfirmDeleteDialogComponentAgree}
                                          onDisagree={this.onConfirmDeleteDialogComponentDisagree}
            />
        </Fragment>)
    }

    componentDidMount() {
        log(`componentDidMount`);
        let requestId = undefined;
        if (isCompany(this.props.user)) {
            requestId = this.props.match.params.id;
        } else {
            requestId = this.props.user.id;
        }
        this.getInitialData(requestId).then(() => log(`componentDidMount get initial data finished`));
    }

    componentWillUnmount() {
        log(`componentDidUnmount`);
        this.props.resetAction();
    }
}

const mapStateToProps = (state) => ({
    user: state.auth.user,
    userInfo: state.user.userInfo,
    countries: state.app.countries,
    availableSkills: state.app.skills,
});

const mapDispatchToProps = (dispatch) => ({
    getUserInfo: (userId) => dispatch(UserActions.getUserInfo(userId)),
    addUserProfile: (user, contactInfo, details, skills) => dispatch(UserActions.addUserProfile(user, contactInfo, details, skills)),
    removeUserSkill: (id) => dispatch(UserActions.removeUserSkill(id)),
    removeUserEducation: (id) => dispatch(UserActions.removeUserEducation(id)),
    removeUserWork: (id) => dispatch(UserActions.removeUserWork(id)),
    getUserSkills: (userId) => dispatch(getUserSkills(userId)),
    resetAction: () => dispatch(resetAction())
});

const withConnect = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(UserProfilePage));

export default withConnect;