import React from 'react';
import {withStyles} from 'material-ui/styles';
import ContactInfoComponent from "../../../../components/ContactInfoComponent";
import UserExperienceComponent from '../experience';
import PreviewComponent from '../preview';
import UserSkillsComponent from '../experience/skills'
import Card from 'material-ui/Card';
import Stepper, {Step, StepContent, StepLabel} from 'material-ui/Stepper';

const styles = {
    root: {},
    div: {
        marginTop: 20
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
    },
    previewCard: {
        display: 'flex',
        flexDirection: 'column',
        alignSelf: 'center',
        margin: 10,
        width: "50%"
    }
};

const ProfileStepperComponent = (props) => {
    const {
        classes,
        companyUser,
        userInfo,
        activeStep,
        onSubmit,
        changeStep,
        countries,
        works,
        educations,
        skills,
        availableSkills,
        contactInfo,
        addProp,
        onValueChange,
        onRemove,
    } = props;

    const steps = ["General Information", "Work", "Education", "Skills", "Preview"];
    // const steps = ["General Information", "Work", "Education", "Preview"];

    const getActiveStep = (step, saveButton = true) => {
        switch (step) {
            case 0:
                return (
                    <Card className={classes.previewCard}>
                        <ContactInfoComponent
                            countries={countries}
                            contactInfo={contactInfo}
                            useValue = {true}
                            onChange={onValueChange('contactInfo')()}
                        />
                    </Card>
                );
            case 1:
                return (
                    <UserExperienceComponent
                        title={"Work Experience:"}
                        addProp={addProp("userDetails")("works")}
                        details={works}
                        onChange={onValueChange('userDetails', 'works')}
                        onRemove={onRemove('userDetails', 'works')}
                    />);
            case 2:
                return (
                    <UserExperienceComponent
                        title={"Education:"}
                        addProp={addProp("userDetails")("educations")}
                        details={educations}
                        onChange={onValueChange('userDetails', 'educations')}
                        onRemove={onRemove('userDetails', 'educations')}
                    />);
            case 3:
                return (
                    <UserSkillsComponent
                        title={"Skill:"}
                        skills={skills}
                        availableSkills={availableSkills}
                        add={addProp()("skills")}
                        onChange={onValueChange("skills")}
                        onRemove={onRemove("skills")}
                        useValue={true}
                    />
                );
            case 4:
                return (
                    <PreviewComponent contactInfo={contactInfo}
                                      educations={educations}
                                      works={works}
                                      skills={skills}
                                      countries={countries}
                                      saveButton={saveButton}
                    />);
            default:
                return null;
        }
    };
    if (companyUser)
        return (
            <div className={classes.div}>
                {getActiveStep(steps.length - 1, false)}
            </div>
        );
    return (<div className={classes.root}>
        {userInfo ?
            <form onSubmit={onSubmit}>
                <Stepper activeStep={activeStep} orientation="vertical">
                    {steps.map((step, index) =>
                        <Step key={index}>
                            <StepLabel onClick={changeStep(index, steps.length - 1)}>
                                {step}
                            </StepLabel>
                            <StepContent>
                                <div className={classes.form}>
                                    {getActiveStep(index)}
                                </div>
                            </StepContent>
                        </Step>
                    )}
                </Stepper>
            </form>
            :
            null
        }
    </div>)
};

export default withStyles(styles)(ProfileStepperComponent);