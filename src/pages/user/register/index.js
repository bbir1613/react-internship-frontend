import React, {Component} from 'react';
import UserRegisterComponent from '../create/register';
import {connect} from 'react-redux';
import {withStyles} from 'material-ui/styles';
import {resetAction, switchRoute} from "../../app/actions";
import * as UserActions from "../actions";
import {LOGIN} from "../../../constants/routes-name";

const styles = {
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignItems:'center',
        height:"calc(100vh - 64px)"
    },
    innerDiv: {
    }
};

class RegisterPage extends Component {
    onAddUserSubmit = (user) => {
        this.props.addUser(user).then(() => {
            this.props.switchRoute(LOGIN);
        });
    };

//TODO(BOGDAN): change the page layout
    render() {
        return (
            <div className={this.props.classes.root}>
                    <UserRegisterComponent
                        onCreateUserSubmit={this.onAddUserSubmit}
                    />
            </div>
        )
    }

    componentWillUnmount() {
        this.props.resetAction();
    }
}

const mapDispatchToProps = (dispatch) => ({
    addUser: (user) => dispatch(UserActions.addUser(user)),
    switchRoute: (route) => dispatch(switchRoute(route)),
    resetAction: () => dispatch(resetAction()),
});


const withConnect = connect(null,mapDispatchToProps)(withStyles(styles)(RegisterPage));

export default withConnect;