import * as UserActionType from '../constants';
import http from "../../../configs/http";
import {USERJOBAPPLICATIONS, USERS} from "../../../constants/api-routes";
import {setError, setNotification} from '../../../components/notification/actions';

import getLogger from '../../../util/logger';

const log = getLogger('user-actions', false);

export const getUsers = () => dispatch => {
    log(`getUsers`);
    return http.get(`${USERS}`)
        .then(response => {
            log(`getUsers success`, response.data);
            dispatch(getUserSucceeded(response.data));
        }).catch(error => {
            log(`getUsers error`, error);
            dispatch(setError(error.response.statusText))
        });
};

export const getUserInfo = (userId) => dispatch => {
    log(`getUserInfo `, userId);
    return http.get(`${USERS}/${userId}/true`)
        .then(response => {
            log(`getUserInfo success`, response.data);
            dispatch(getUserInfoSucceeded(response.data));
            return response.data;
        }).catch(error => {
            log(`getUserInfo error`, error);
            dispatch(setError(error.response.statusText))
        });
};

export const getUsersThatAppliedForAJob = (jobId) => dispatch => {
    log(`getUserAppliedForJob`);
    return http.get(`${USERJOBAPPLICATIONS}/job/${jobId}`)
        .then(response => {
            log(`getUserAppliedForJob success`, response.data);
            const users = response.data.filter(user => !user.isAccepted);
            dispatch(getUserSucceeded(users));
        }).catch(error => {
            log(`getUserAppliedForJob error`, error);
            dispatch(setError(error.response.statusText));
        })
};

export const addUser = (user) => dispatch => {
    log(`addUser`);
    return http.post(`${USERS}`, user).then(response => {
        log(`addUser succes`, response.data);
        dispatch(setNotification(`User added with success`));
    }).catch(error => {
        log(`addUser error`, error);
        dispatch(setError(error.response.statusText));
    })

};

export const updateUser = (user) => dispatch => {
    log(`updateUser`);
    return http.put(`${USERS}/${user.id}`, user).then(response => {
        log(`updateUser succes`, response.data);
        dispatch(setNotification(`User updated with success`));
    }).catch(error => {
        log(`updateUser error`, error);
        dispatch(setError(error.response.statusText))
    })
};

export const addUserProfile = (user, contactInfo, details, skills) => async dispatch => {
    try {
        log(`addUserProfile`);
        if (!contactInfo.id) {
            const ci = await http.post('/contacts', contactInfo);
            log(`addUserProfile`, ci);
            const us = await http.put(`${USERS}/${user.id}`, {...user, contactInfoId: ci.data.id});
            log(`addUserProfile`, ` update user `, us);
        }
        else {
            log(`update contactinfo`);
            await http.put(`/contacts/${contactInfo.id}`, contactInfo);
        }
        for (let i = 0; i < details.works.length; ++i) await addUserWork(details.works[i])
        for (let i = 0; i < details.educations.length; ++i) await addUserEducation(details.educations[i])
        for (let i = 0; i < skills.length; ++i) await addUserSkill(skills[i])
        dispatch(setNotification("Profile saved with success"));
    } catch (error) {
        log(`addUserProfile error`, error);
        dispatch(setError(error.response.statusText))
    }
};

export const removeUserSkill = (id) => dispatch => {
    log(` removeUserSkill `, id);
    return http.delete(`/userskills/${id}`);
};

export const removeUserEducation = (id) => dispatch => {
    log(` removeUserEducation `, id);
    return http.delete(`/usereducations/${id}`);
};

export const removeUserWork = (id) => dispatch => {
    log(` removeUserWork `, id);
    return http.delete(`/userworkexperiences/${id}`);
};

export const addUserSkill = (skill) => {
    if (skill.id) {
        return http.put(`/userskills/${skill.id}`, skill);
    }
    return http.post('/userskills', skill);
};

export const addUserEducation = (userEdutcation) => {
    if (userEdutcation.id) {
        return http.put(`/usereducations/${userEdutcation.id}`, userEdutcation);
    }
    return http.post('/usereducations', userEdutcation);
};

export const addUserWork = (userWork) => {
    if (userWork.id) {
        return http.put(`/userworkexperiences/${userWork.id}`, userWork);
    }
    return http.post('/userworkexperiences', userWork);
};

export const deleteUser = (userId) => dispatch => {
    log(`deleteUser`);
    return http.delete(`${USERS}/${userId}`)
        .then(response => {
            log(`deleteUser succes`, response.data);
            dispatch(setNotification(`User deleted with success`));
        }).catch(error => {
            log(`deleteUser error`, error);
            dispatch(setError(error.response.statusText));
        })
};

const getUserSucceeded = (payload) => ({type: UserActionType.GET_USERS_SUCCEEDED, payload});
const getUserInfoSucceeded = (payload) => ({type: UserActionType.GET_USER_INFO, payload});