import * as UserTypeActions from '../constants';
import {RESET_ACTION} from "../../app/constants";

let reduce = {};

reduce[UserTypeActions.GET_USERS_SUCCEEDED] = (state, action) => ({...state, users: action.payload});
reduce[UserTypeActions.GET_USER_INFO] = (state, action) => ({...state, userInfo: action.payload});
reduce[RESET_ACTION] = (state, action) => (initialState());

const initialState = ()=>({
    users: [],
    userInfo: undefined
});

const reducer = (state = initialState(), action) => reduce[action.type] ? reduce[action.type](state, action) : state;

export default reducer;