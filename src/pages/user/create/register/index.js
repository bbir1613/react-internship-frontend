import React from 'react';
import {withStyles} from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Card from "material-ui/Card";
import Button from 'material-ui/Button';
import getLogger from '../../../../util/logger';
import {USER} from "../../../../constants/roles";

const log = getLogger("UserCreateComponent");

const styles = {
    root: {
        width: '100%',
        minHeight: "calc(100vh - 64px)",
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    card: {
        minWidth: '25%'
    },
    content: {
        margin: 25,
        display: 'flex',
        flexDirection: 'column'
    },
    button: {
        minWidth: '15%'
    },
    action: {
        margin: 10,
        padding: 15,
        display: 'flex',
        flexDirection: 'row-reverse'
    },
    toolbar: {
        display: 'flex',
        justifyContent: 'center',
        backgroundColor: '#3f51b5',
        color: 'white'
    },
};

const UserRegisterComponent = (props) => {
    const {classes, onCreateUserSubmit} = props;

    const onSubmit = (event) => {
        event.preventDefault();
        log("nothing");
        const username = event.target.username.value;
        const firstName = event.target.firstName.value;
        const lastName = event.target.lastName.value;
        const password = event.target.password ? event.target.password.value : undefined;
        const userRoleId = USER;
        log(`onSubmit`, username, firstName, lastName, password, userRoleId);
        onCreateUserSubmit({
            username,
            firstName,
            lastName,
            password,
            userRoleId
        });
    };
    return (
        <div className={classes.root}>
            <Card className={classes.card}>
                <Toolbar className={classes.toolbar}>
                    <Typography variant="title" color="inherit">
                        Sign up
                    </Typography>
                </Toolbar>
                <form className={classes.form} onSubmit={onSubmit}>
                    <div className={classes.content}>
                        <TextField className={classes.input} label='username' name='username'/>
                        <TextField className={classes.input} label='firstName' name='firstName'/>
                        <TextField className={classes.input} label='lastName' name='lastName'/>
                        <TextField className={classes.input} label='password' type="password" name='password'/>
                    </div>
                    <div className={classes.action}>
                        <Button className={classes.button} color='primary' variant='raised'
                                type="submit"> Register </Button>
                    </div>
                </form>
            </Card>
        </div>
    )
};

export default withStyles(styles)(UserRegisterComponent);