import React, {Fragment} from 'react';
import Paper from 'material-ui/Paper';
import {withStyles} from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import getLogger from '../../../util/logger';

const log = getLogger("UserCreateComponent");

const styles = {
    root: {},
    paper: {
        marginTop: 10,
        marginBottom: 10,
        display: 'flex',
        minWidth: '20%'
    },
    form: {
        width: '100%'
    },
    input: {},
    content: {
        display: 'flex',
        flexDirection: 'column',
        margin: 10,
    },
    action: {
        margin: 10,
        display: 'flex',
        flexDirection: 'row-reverse'
    },
    button: {
        margin: 5
    },
};

const UserCreateComponent = (props) => {
    const {classes, onCreateUserSubmit, roles, onCancel, open, user} = props;

    const onSubmit = (event) => {
        event.preventDefault();
        const username = event.target.username.value;
        const firstName = event.target.firstName.value;
        const lastName = event.target.lastName.value;
        const password = event.target.password ? event.target.password.value : undefined;
        const userRoleId = event.target.userRoleId ? event.target.userRoleId.value : undefined;
        const id = user ? user.id : undefined;
        log(`onSubmit`, username, firstName, lastName, password, userRoleId);
        onCreateUserSubmit({
            id,
            username,
            firstName,
            lastName,
            password,
            userRoleId
        });
    };
    return (
        <Fragment>
            {open ?
                <Paper className={classes.paper}>
                    <form className={classes.form} onSubmit={onSubmit}>
                        <div className={classes.content}>
                            <TextField className={classes.input} label='username' name='username'
                                       defaultValue={user ? user.username : ''}/>
                            <TextField className={classes.input} label='firstName' name='firstName'
                                       defaultValue={user ? user.firstName : ''}/>
                            <TextField className={classes.input} label='lastName' name='lastName'
                                       defaultValue={user ? user.lastName : ''}/>
                            {!user ? <TextField className={classes.input} label='password' name='password'/> : null}
                            {!user ? <TextField className={classes.input} select label='userRoleId' name='userRoleId'
                                                SelectProps={{
                                                    native: true,
                                                    MenuProps: {
                                                        className: classes.menu,
                                                    },
                                                }}>
                                    {roles.map((role, index) => {
                                        return (
                                            <option key={role.id} value={role.id}>
                                                {role.name}
                                            </option>
                                        )
                                    })}
                                </TextField>
                                :
                                null}
                        </div>
                        <div className={classes.action}>
                            <Button className={classes.button} color='primary' variant='raised'
                                    type="submit"> {user ? 'Edit' : 'Create'} </Button>
                            <Button className={classes.button} color='primary' variant='raised'
                                    onClick={onCancel}
                                    type="button"> {'Cancel'} </Button>
                        </div>
                    </form>
                </Paper>
                :
                null}
        </Fragment>
    )
};

export default withStyles(styles)(UserCreateComponent);