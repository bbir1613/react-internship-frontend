import React, {Component} from 'react';
import {withStyles} from 'material-ui/styles';
import {connect} from 'react-redux';
import getLogger from '../../util/logger';
import JobListCardComponent from '../job/list/card';
import SearchComponent from './search'
import {resetAction, switchRoute} from "../app/actions";
import {filterJobsByCompanyName, getAllJobs} from "../job/actions";
import {getAllCompanies} from "../company/actions";

const log = getLogger("UserPage");

const styles = {
    root: {
        display: 'flex',
        justifyContent: 'center'
    }
};

class UserPage extends Component {

    state = {
        selectedCompanies: []
    };

    getFilteredJobs = () => {
        if (this.state.selectedCompanies.length === 0) {
            this.props.getAllJobs();
        } else {
            this.props.filterJobsByCompanyName(this.state.selectedCompanies);
        }
    };

    onChange = (event) => {
        this.setState({...this.state, selectedCompanies: event.target.value}, this.getFilteredJobs)
    };

    onCompanyNameClick = (company) => {
        const index = this.state.selectedCompanies.findIndex((selectedCompany) => selectedCompany === company);
        if (index === -1) {
            this.setState(prevState => ({
                ...this.state,
                selectedCompanies: prevState.selectedCompanies.concat(company)
            }), this.getFilteredJobs)
        } else {
            const selectedCompanies = [...this.state.selectedCompanies];
            selectedCompanies.splice(index, 1);
            this.setState({
                ...this.state,
                selectedCompanies: selectedCompanies
            }, this.getFilteredJobs)
        }
    };

    render() {
        const {classes} = this.props;
        return <div className={classes.root}>
            <JobListCardComponent
                jobs={this.props.jobs}
                onClick={this.onCompanyNameClick}
                switchRoute={this.props.switchRoute}
            />
            <SearchComponent
                companies={this.props.companies}
                selected={this.state.selectedCompanies}
                onChange={this.onChange}
            />
        </div>
    }

    componentDidMount() {
        log(`componentDidMount`);
        this.props.getAllJobs();
        this.props.getAllCompanies();
    }

    componentWillUnmount() {
        log(`componentWillUnmount`);
        this.props.resetAction();
    }
}

const mapStateToProps = (state) => ({
    jobs: state.job.jobs,
    companies: state.company.companies,
});

const mapDispatchToProps = (dispatch) => ({
    getAllJobs: () => dispatch(getAllJobs()),
    getAllCompanies: () => dispatch(getAllCompanies()),
    filterJobsByCompanyName: (companiesName) => dispatch(filterJobsByCompanyName(companiesName)),
    switchRoute: (path) => dispatch(switchRoute(path)),
    resetAction: () => dispatch(resetAction())
});

const withConnect = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(UserPage));

export default withConnect;