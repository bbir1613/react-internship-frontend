import React, {Fragment} from 'react';
import Table, {TableBody, TableCell, TableHead, TableRow} from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import DeleteIcon from 'material-ui-icons/Delete';
import EditIcon from 'material-ui-icons/Edit';
import Button from 'material-ui/Button';
import Divider from 'material-ui/Divider';
import Typography from 'material-ui/Typography';
import {withStyles} from 'material-ui/styles';

const styles = {
    root: {
        marginTop: 10,
        marginBottom: 10,
        // marginTop: 35,
    },
    table: {
        // padding: 5,
    },
    content: {
        margin: 20
    },
    typography: {
        margin: 10
    }
};


const mapUserRole = (roles, userRoleId) => {
    const role = roles.find(role => role.id === userRoleId);
    if (role) return role.name;
    return null;
};

const UserListComponent = (props) => {
    const {classes, users, roles, onDeleteUser, onEditUser} = props;
    return (
        <Fragment>
            {users.length ?
                <Paper className={classes.root}>
                    <div className={classes.content}>
                        <Typography variant="display2" className={classes.typography}> User list</Typography>
                        <Divider/>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Username</TableCell>
                                    <TableCell>FirstName</TableCell>
                                    <TableCell>LastName</TableCell>
                                    <TableCell>Role</TableCell>
                                    <TableCell/>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {users.map((user, index) => {
                                    return (
                                        <TableRow key={index}>
                                            <TableCell> {user.username} </TableCell>
                                            <TableCell> {user.firstName}</TableCell>
                                            <TableCell> {user.lastName}</TableCell>
                                            <TableCell> {mapUserRole(roles, user.userRoleId)} </TableCell>
                                            <TableCell>
                                                <Button size="small"
                                                        onClick={() => onDeleteUser(user)}><DeleteIcon/></Button>
                                                <Button size="small"
                                                        onClick={() => onEditUser(user)}><EditIcon/></Button>
                                            </TableCell>
                                        </TableRow>
                                    )
                                })}
                            </TableBody>
                        </Table>
                    </div>
                </Paper>
                :
                null}
        </Fragment>)
};

export default withStyles(styles)(UserListComponent);