import React, {Component} from 'react';
import {withStyles} from 'material-ui/styles';
import {connect} from "react-redux";
import getLogger from '../../../../util/logger';
import {resetAction, switchRoute} from "../../../app/actions";
import {getJobsUserAppliedFor} from "../../../job/actions";
import TitleComponent from '../../../../components/TitleComponent';
import JobsUserAppliedForListComponent from '../../../job/list/userapplied';

const log = getLogger("UserJobsAppliedPage");

const styles = {
    root: {},
    content: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    }
};

class UserJobsAppliedPage extends Component {

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <TitleComponent title="Applied jobs"/>
                <div className={classes.content}>
                    <JobsUserAppliedForListComponent
                        appliedJobs={this.props.appliedJobs}
                        switchRoute={this.props.switchRoute}
                    />
                </div>
            </div>
        )
    }

    componentDidMount() {
        log(`componentDidMount`);
        this.props.getJobsUserAppliedFor(this.props.user.id);
    }

    componentWillUnmount() {
        log(`componentWillUnmount`);
        this.props.resetAction();
    }
}

const mapStateToProps = (state) => ({
    user: state.auth.user,
    appliedJobs: state.job.appliedJobs,
});

const mapDispatchToProps = (dispatch) => ({
    getJobsUserAppliedFor: (userId) => dispatch(getJobsUserAppliedFor(userId)),
    switchRoute: (route) => dispatch(switchRoute(route)),
    resetAction: () => dispatch(resetAction)
});

const withConnect = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(UserJobsAppliedPage));

export default withConnect;