import React, {Component} from 'react';
import {withStyles} from 'material-ui/styles';
import {getJobInfoById, userApplyToJob} from "../../job/actions";
import {connect} from "react-redux";
import getLogger from '../../../util/logger';
import JobDetailComponent from '../../job/detail';
import ConfirmApplyJobDialogComponent from '../../../components/ConfirmDialogComponent';
import {resetAction} from "../../app/actions";

const log = getLogger("UserJobDetailsPage");

const styles = {};

class UserJobDetailsPage extends Component {

    state = {
        selectedJob: undefined,
        openConfirmApplyJobDialogComponent: false
    };

    onApplyJobRequest = (job) => {
        log(`onApplyJobRequest`, job);
        this.setState({openConfirmApplyJobDialogComponent: true, selectedJob: {...job}});
    };

    onConfirmApplyJobDialogComponentDisagree = () => {
        log(`onConfirmApplyJobDialogComponentDisagree`);
        this.setState({openConfirmApplyJobDialogComponent: false, selectedJob: undefined});
    };

    onConfirmApplyJobDialogComponentAgree = () => {
        log(`onConfirmApplyJobDialogComponentAgree`);
        this.props.userApplyToJob(this.state.selectedJob.id, this.props.user.id).then(() => {
            this.props.getJobInfoById(this.props.match.params.id);
        });
        this.setState({openConfirmApplyJobDialogComponent: false, selectedJob: undefined});
    };


    render() {
        return (
            <div>
                <JobDetailComponent job={this.props.jobInfo}
                                    user={this.props.user}
                                    onApplyJob={this.onApplyJobRequest}
                />
                <ConfirmApplyJobDialogComponent open={this.state.openConfirmApplyJobDialogComponent}
                                                onAgree={this.onConfirmApplyJobDialogComponentAgree}
                                                onDisagree={this.onConfirmApplyJobDialogComponentDisagree}
                                                title="Are you sure you want to apply for current job"
                                                info={this.state.selectedJob ? this.state.selectedJob.name : ''}
                />
            </div>
        )
    }

    componentDidMount() {
        log(`componentDidMount`);
        this.props.getJobInfoById(this.props.match.params.id);
    }

    componentWillUnmount() {
        log(`componentWillUnmount`);
        this.props.resetAction();
    }
}

const mapStateToProps = (state) => ({
    jobInfo: state.job.jobInfo,
    user: state.auth.user,
});

const mapDispatchToProps = (dispatch) => ({
        getJobInfoById: (jobId) => dispatch(getJobInfoById(jobId)),
        userApplyToJob: (jobId, userId) => dispatch(userApplyToJob(jobId, userId)),
        resetAction: () => dispatch(resetAction)
    }
);

const withConnect = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(UserJobDetailsPage));

export default withConnect;