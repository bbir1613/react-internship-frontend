import React, {Fragment} from 'react';
import {FormControl} from 'material-ui/Form';
import Select from 'material-ui/Select';
import Input, {InputLabel} from 'material-ui/Input';
import Checkbox from 'material-ui/Checkbox';
import {ListItemText} from 'material-ui/List';
import {MenuItem} from 'material-ui/Menu';
import {withStyles} from 'material-ui/styles';

const styles = {
    root: {},
    formControl: {
        marginTop: 20,
        minWidth: '20%'
    }
};

const SearchComponent = (props) => {
    const {classes, companies, selected, onChange} = props;

    return (<Fragment>
            <FormControl className={classes.formControl}>
                <InputLabel htmlFor="select-multiple-checkbox">Filter by company name</InputLabel>
                <Select
                    multiple={true}
                    value={selected}
                    onChange={onChange}
                    input={<Input id="select-multiple"/>}
                    renderValue={selected => selected.join(" ")}
                >
                    {companies.map((company, index) => (
                        <MenuItem key={index} value={company.name}>
                            <Checkbox checked={selected.indexOf(company.name) > -1}/>
                            <ListItemText primary={company.name}/>
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
        </Fragment>
    )
};

export default withStyles(styles)(SearchComponent);