import * as AppTypeActions from '../constants';

let reduce = {};

reduce[AppTypeActions.GET_ROLES_SUCCEEDED] = (state, action) => ({...state, roles: action.payload});
reduce[AppTypeActions.GET_COUNTRIES_SUCCEEDED] = (state, action) => ({...state, countries: action.payload});
reduce[AppTypeActions.GET_SKILLS_SUCCEEDED] = (state, action) => ({...state, skills: action.payload});

const initialState = ()=>({
    roles: [],
    countries: [],
    skills:[]
});

const reducer = (state = initialState(), action) => reduce[action.type] ? reduce[action.type](state, action) : state;

export default reducer;