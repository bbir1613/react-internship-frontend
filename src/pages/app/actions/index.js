import http from "../../../configs/http";
import * as AppTypeActions from '../constants';
import getLogger from '../../../util/logger';
import {setError} from '../../../components/notification/actions';
import {getUser} from "../../../util/storage";
import {loginSucceeded} from "../../../components/login/actions";
import {push} from 'react-router-redux'

const log = getLogger("app-actions");

export const getRoles = () => dispatch => {
    log(`getRoles`);
    return http.get('/userroles')
        .then(response => {
            log(`getRoles success`, response.data);
            dispatch(getRolesSucceeded(response.data));
        }).catch(error => {
            log(`getRoles error`, error);
            dispatch(setError(error.response.statusText));
            alert(error);
        });

};

export const getCountries = () => dispatch => {
    log(`getCountries`);
    return http.get('/countries')
        .then(response => {
            log(`getRoles success`, response.data);
            dispatch(getCountriesSucceeded(response.data));
        }).catch(error => {
            log(`getRoles error`, error);
            dispatch(setError(error.response.statusText));
        });

};

export const getSkills = () => dispatch => {
    log(`getSkills`);
    return http.get('/skills')
        .then(response => {
            log(`getSkills success`, response.data);
            dispatch(getSkillsSucceeded(response.data));
        }).catch(error => {
            log(`getSkills error`, error);
            dispatch(setError(error.response.statusText));
        });
};

export const getUserSkills = (userId) => dispatch => {
    log(`getUserSkills`, userId);
    return http.get(`/userskills/user/${userId}`).then(response => response.data).catch(error => {
        log(`getUserSkills error`, error);
        dispatch(setError(error.response.statusText));
    });
};

export const addSkill = (skill) => dispatch => {
    log(`addSkill`, skill);
    let request = skill.id ? http.put(`/skills/${skill.id}`, skill) : http.post('/skills', skill);
    return request.then(response => {
        log(`addSkill success`, response.data);
        dispatch(getSkills());
    }).catch(error => {
        log(`addSkill error`, error);
        dispatch(setError(error.response.statusText));
    });
};

export const deleteSkill = (skillId) => dispatch => {
    log(`deleteSkill`, skillId);
    return http.delete(`/skills/${skillId}`)
        .then(response => {
            log(`deleteSkill success`, response.data);
            dispatch(getSkills());
        }).catch(error => {
            log(`deleteSkill error`, error);
            dispatch(setError(error.response.statusText));
        });
};

export const onAppInit = () => dispatch => {
    const user = getUser();
    log(`onAppInit`, user);
    if (user) {
        dispatch(loginSucceeded({...user, isLoggedIn: true}));
    }
};

export const switchRoute = (route) => {
    return push(route);
};

export const resetAction = () => ({type: AppTypeActions.RESET_ACTION});

const getRolesSucceeded = (payload) => ({type: AppTypeActions.GET_ROLES_SUCCEEDED, payload});
const getCountriesSucceeded = (payload) => ({type: AppTypeActions.GET_COUNTRIES_SUCCEEDED, payload});
const getSkillsSucceeded = (payload) => ({type: AppTypeActions.GET_SKILLS_SUCCEEDED, payload});