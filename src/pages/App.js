import React, {Component} from 'react';
import LayoutComponent from '../components/LayoutComponent';
import NotificationsComponent from '../components/notification';
import {createMuiTheme, MuiThemeProvider} from 'material-ui/styles';
import {ConnectedRouter} from 'react-router-redux';
import history from '../configs/history';
import {translate} from 'react-i18next';
import {connect} from 'react-redux';
import * as AppActions from './app/actions';
import getLogger from '../util/logger';

const theme = createMuiTheme();

const log = getLogger("App");

class App extends Component {

    componentWillMount() {
        log(`componentWillMount`);
        this.props.onAppInit();
    }

    render() {
        return (
            <ConnectedRouter history={history}>
                <MuiThemeProvider theme={theme}>
                    <LayoutComponent/>
                    <NotificationsComponent/>
                </MuiThemeProvider>
            </ConnectedRouter>
        );
    }

    componentDidMount() {
        log(`componentDidMount`);
        this.props.getRoles();
        this.props.getCountries();
        this.props.getSkills();
    }

    componentWillUnmount() {
        log(`componentDidUnmount`);
        this.props.resetAction();
    }
}

const mapDispatchToProps = (dispatch) => ({
    getRoles: () => dispatch(AppActions.getRoles()),
    getCountries: () => dispatch(AppActions.getCountries()),
    getSkills: () => dispatch(AppActions.getSkills()),
    onAppInit: () => dispatch(AppActions.onAppInit()),
    resetAction: () => dispatch(AppActions.resetAction())
});

const withConnect = connect(null, mapDispatchToProps)(translate('translations')(App));

export default withConnect;