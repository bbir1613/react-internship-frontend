import React from 'react';
import TextField from 'material-ui/TextField';

const renderTextField = ({input, meta, ...custom}) => <TextField {...input} {...custom}/>;

export default renderTextField;
