const USER_TAG = "user";
const LANGUAGE_TAG = "language";

export const saveUser = (user)=>{
    sessionStorage.setItem(USER_TAG, JSON.stringify({...user}));
};

export const getUser = ()=>{
    const user = sessionStorage.getItem(USER_TAG);
    if(user) return JSON.parse(user);
    return undefined;
};

export const removeUser = ()=>{
    sessionStorage.removeItem(USER_TAG);
};

export const saveLanguage = (language)=>{
    sessionStorage.setItem(LANGUAGE_TAG, language);
}

export const getLanguage = ()=>{
    return sessionStorage.getItem(LANGUAGE_TAG);
}