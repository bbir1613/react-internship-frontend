const getLogger = (TAG, debug = false) => (...message) => debug ? console.log(TAG, ':\t', ...message) : null;

export default getLogger;