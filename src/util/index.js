import _ from 'lodash';

export const removeFromObject = async (object, path, request) => {
    const obj = _.get(object, path);
    if (obj.id) {
        const type = path[path.length - 2];
        if (!request[type]) console.error("Invalid type", type);
        await request[type](obj.id);
    }
    const index = path.pop();
    const lst = [..._.get(object, path)];
    lst.splice(index, 1);
    _.set(object, path, lst);
    return object;
};