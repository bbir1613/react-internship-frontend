import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {routerReducer, routerMiddleware} from 'react-router-redux';
import {reducer as formReducer} from 'redux-form'
import history from '../history';

import authReducer from '../../components/login/reducer';
import notificationReducer from '../../components/notification/reducer';
import appReducer from '../../pages/app/reducer';
import userReducer from '../../pages/user/reducer';
import companyReducer from '../../pages/company/reducer';
import jobReducer from '../../pages/job/reducer';

const rootReducers = combineReducers({
    auth: authReducer,
    notification: notificationReducer,
    app: appReducer,
    user: userReducer,
    company: companyReducer,
    job: jobReducer,
    form: formReducer,
    router: routerReducer
});

const enhancers = [thunk, routerMiddleware(history)];

const enhance = applyMiddleware(...enhancers);

const store = createStore(rootReducers, enhance);

export default store;