import {Switch} from 'react-router-dom';
import {connect} from 'react-redux';

const mapStateToProps = (state)=>({
    location: state.location
});

const withConnect = connect(mapStateToProps, null)(Switch);

export default withConnect;